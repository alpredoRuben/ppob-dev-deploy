<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class BPJS extends REST_Controller
{
    /** Instanced To Set Response Status Event */
    private $EVENT_STATUS = false;

    /** Instanced To Set Session Value */
    private $OPTIONAL_SESSION = "";

    /** Instanced To Set Response HTTP {Inquiry, Payment, Reversal Or Committed} */
    private $RESPONSE_MESSAGES = "";

    /** Instance To Set Product Code */
    private $PRODUCT_CODE = "";

    /** Instance To Set Product Name */
    private $PRODUCT_NAME = "";

    /** Instance To Set Fee From Admin Bank  */
    private $ADMIN_BANK_FEE = "";

    /** Constructor */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('fortuna/bpjslib');
    }

    /** Function To Check Session */
    public function CheckSession($session_names)
    {
        if($this->session->userdata($session_names))
            return true;

        return false;
    }

    /** Function To Create Session Event */
    public function CreateSession($name, $id, $fill)
    {
        if($this->CheckSession($name."_".$id))
            $this->session->unset_userdata($name."_".$id);

        $this->session->set_userdata($name."_".$id, $fill);
        $session = $this->session->userdata($name."_".$id);

        return $session;
    }

    /** Function To Set And Return Response Value With Args */
    public function ApiResponse($status, $json, $optional=null, $method)
    {
        $data = array("STATUS" => $status, "RESULTS" => $json, "OPTIONAL" => $optional);

        switch ($method) {
            case 404:
                return $this->response($data, REST_Controller::HTTP_NOT_FOUND);
                break;
            case 200:
                return $this->response($data, REST_Controller::HTTP_OK);
                break;
            default:
                return $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
                break;
        }
    }

    /** Rest Server API Service Request Inquiry BPJS Kesehatan */
    public function inquiry_get($inisial, $customer_id, $periode)
    {
        $bpjsInquiry = array();

        $this->ADMIN_BANK_FEE = GetBPJSAdminFee($inisial);
        $this->PRODUCT_CODE = GetBPJSCode($inisial);
        $this->PRODUCT_NAME = GetBPJSNames($inisial);

        $INQ = $this->bpjslib->GetBPJSInquiry($customer_id, $this->PRODUCT_CODE, $periode);

        $bpjsInquiry["KATEGORI_BPJS"]        = $this->PRODUCT_NAME;
        $bpjsInquiry["RESPONS_HTTP_INQUIRY"] = $INQ["response_http"];
        $bpjsInquiry["STATUS_TRANSAKSI"]     = "INQUIRY";

        if($INQ["response_http"] == "" || $INQ["response_http"] == false)
        {
            $this->EVENT_STATUS = false;
            $bpjsInquiry["PESAN_KESALAHAN"]  = $INQ["error_message"];
        }
        else
        {
            $data_iso  = $INQ["data_iso"];
            $error_msg = $data_iso["39"];

            $bpjsInquiry["PESAN_BIT_ISO"]    = $INQ["messages_iso"];
            $bpjsInquiry["KODE_KESALAHAN"]   = $error_msg;

            $this->OPTIONAL_SESSION = $this->CreateSession($inisial, $customer_id, $INQ["respon_http"]);

            if($error_msg == "00")
            {
                $this->EVENT_STATUS = true;
                $bpjsInquiry["PESAN_KESALAHAN"] = $this->bpjslib->GetErrorMessage($error_msg);
                $bpjsInquiry["DATA_EKSTRAKSI"]  = $this->bpjslib->GetArrayBPJSKesehatan($data_iso["48"], $this->admin_fee);
            }
            else
            {
                $this->EVENT_STATUS = false;
                $bpjsInquiry["PESAN_KESALAHAN"] = $this->bpjslib->GetErrorMessage($error_msg);
            }

        }

        $this->ApiResponse($this->EVENT_STATUS, $bpjsInquiry, $this->OPTIONAL_SESSION, 200);

    }

    /** Rest Server API Service Request Payment */
    public function payment_get($inisial, $customer_id, $periode)
    {
        $bpjsPayment  = array();

        $this->ADMIN_BANK_FEE = GetBPJSAdminFee($inisial);
        $this->PRODUCT_CODE = GetBPJSCode($inisial);
        $this->PRODUCT_NAME = GetBPJSNames($inisial);

        if( $this->CheckSession($inisial."_".$customer_id) )
        {
            $this->EVENT_STATUS = false;
            $bpjsPayment['PESAN_KESALAHAN'] = "Proses Pembayaran Gagal. No Pelanggan ". $customer_id . " Tidak Masuk Dalam Cart";
        }
        else
        {
            $this->OPTIONAL_SESSION = $this->session->userdata($inisial."_".$customer_id);
            $PAY = $this->bpjslib->GetBPJSPayment($this->PRODUCT_CODE, $this->OPTIONAL_SESSION);

            $this->RESPONSE_MESSAGES = $PAY["response_http"];

            $bpjsPayment["RESPONS_HTTP_PAYMENT"] = $this->RESPONSE_MESSAGES;
            $bpjsPayment["STATUS_TRANSAKSI"]     = "PAYMENT";

            if($PAY["response_http"] == "" || $PAY["response_http"] == false)
            {
                $this->EVENT_STATUS = false;
                $bpjsPayment["PESAN_KESALAHAN"] = $PAY["error_message"];
            }
            else
            {
                $data_iso = $PAY["data_iso"];
                $error_msg = $data_iso["39"];

                if($error_msg == "00")
                {
                    $bpjsPayment = $this->CommitPayment($this->RESPONSE_MESSAGE, $this->PRODUCT_CODE, $bpjsPayment);
                }
                else
                {
                    if($error_msg == "86")
                        $bpjsPayment = $this->ReversalPayment($this->OPTIONAL_SESSION, $this->PRODUCT_CODE, $bpjsPayment);

                }
            }
        }

        $this->ApiResponse($this->EVENT_STATUS, $bpjsPayment, null, 200);

    }

    /** Function To Run Process Get Reversal Payment */
    public function ReversalPayment($http_inq, $pCode, $bpjsReversal)
    {
        $REV = $this->bpjslib->GetReversalBPJS($http_inq, $pCode);

        $this->RESPONSE_MESSAGES = $REV["response_http"];

        $bpjsReversal["RESPONSE_HTTP_REVERSAL"] = $this->RESPONSE_MESSAGES;
        $bpjsReversal["STATUS_TRANSAKSI"] = "PAYMENT REVERSAL";

        if($REV["response_http"] == "" || $REV["response_http"] == false)
        {
            $this->status = false;
            $bpjsReversal['PESAN_KESALAHAN'] = $REV["error_message"];
        }
        else
        {
            $bpjsReversal = $this->CommitPayment($this->RESPONSE_MESSAGES, $pCode, $bpjsReversal, "REVERSAL");
        }

        return $bpjsReversal;
    }


    /** Function To Commit Process Response Payment */
    public function CommitPayment($http_pay, $pCode, $bpjsCommit, $desc="")
    {
        $COMM = $this->bpjslib->GetBPJSCommitPayment($http_pay, $pCode);

        $this->RESPONSE_MESSAGES = $COMM["response_http"];

        $bpjsCommit["RESPONSE_HTTP_COMMIT"] = $this->RESPONSE_MESSAGES;
        $bpjsCommit["STATUS_TRANSAKSI"] = ($desc!="")? "PAYMENT COMMIT" : "PAYMENT COMMIT ".$desc;

        if($COMM["response_http"] == "" || $COMM["response_http"] == false)
        {
            $this->EVENT_STATUS = false;
            $bpjsCommit["PESAN_KESALAHAN"] = $COMM["error_message"];
        }
        else
        {
            $data_iso  = $COMM["data_iso"];
            $error_msg = $data_iso["39"];

            $bpjsCommit["KODE_KESALAHAN"] = $error_msg;

            if($error_msg == "00")
            {
                $this->EVENT_STATUS = true;
                $ekstraksi = $this->bpjslib->GetArrayBPJSKesehatan($data_iso["48"], $this->ADMIN_BANK_FEE);
                $bpjsCommit["PESAN_KESALAHAN"] = $this->bpjslib->GetErrorMessage($error_msg);
                $bpjsCommit["DATA_EKSTRAKSI"]  = $ekstraksi;
            }
            else
            {
                $this->EVENT_STATUS = false;
                $data["PESAN_KESALAHAN"] = $this->bpjslib->GetErrorMessage($error_msg);
            }
        }

        return $bpjsCommit;
    }

}
