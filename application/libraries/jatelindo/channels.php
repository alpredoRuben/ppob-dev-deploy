<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Channels
{
    protected $production = array(
        "10" => array("ip" => "202.152.60.60",    "port" => 9933);
        "20" => array("ip" => "202.152.60.60",    "port" => 9933),
        "30" => array("ip" => "202.152.60.61",    "port" => 3171, "timeout" => 15),
        "44" => array("ip" => "182.23.53.50",     "port" => 1423)
        "50" => array("ip" => "10.23.45.1",       "port" => 21906, "timeout" => 40),
        "60" => array("ip" => "119.2.66.36",      "port" => 7099),
        "70" => array("ip" => "183.91.67.198",    "port" => 7101),
        "80" => array("ip" => "202.162.223.154",  "port" => 45002),
        "90" => array("ip" => "202.152.22.116",   "port" => 6014)
    );
    protected $port = null;
    protected $hostname = null;
    protected $timeout = 55;

    /** Connect To Web Socket **/
    public function openConnectionSocket($hostname, $port, $errno, $errstr, $timeout)
    {
        $fp = fsockopen($hostname, $port, $errno, $errstr, $timeout);
        return $fp;
    }

    public function getChannelResponse($message, $mode)
    {
        $response = null;
        $modifier = $this->production[$mode];
        $message  = $message. "\n";

        $this->hostname = $modifier["ip"];
        $this->port = $modifier["port"];

        if(isset($modifier["timeout"]))
            $this->timeout = $modifier["timeout"];


        $socket = $this->openConnectionSocket($this->hostname, $this->port, $errno, $errstr, $this->timeout);

        if (!$fp)
        {
            $response = "!Koneski Ke Server Terputus";
        }
        else
        {
            fwrite($socket, $message);
			$response = fread($socket, 1024) ;
			fclose($socket) ;

        }

        return $response;

    }

}
