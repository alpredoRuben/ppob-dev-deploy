<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH. "/libraries/JAK8583/JAK8583.php";

class Pdamlib
{
    /** IP & Port [Production = "8008"] dan [Development = "4000"] */
    protected $IP_PORT = "http://localhost:4000?q=";

    /** Bit Code 11- System Trace Audit Number */
    protected $AUDIT_NUMBER = 0;

    /** Bit Code 7 - Transmission Date Time */
    protected $TRANSMISSION_DATETIME = null;

    /** Merchant Code Bit 18 */
    protected $MERCHANT_CODE = array(
        "teller"      => "6010",
        "atm"         => "6011",
        "edc"         => "6012",
        "auto_debit"  => "6013",
        "e_banking"   => "6014",
        "kiosk"       => "6015",
        "phone_bank"  => "6016",
        "mobile_bank" => "6017"
    );

    /** Code Bit 32 - Dynamic Code */
    protected $DYNAMIC_CODE = null;

    /** Code Bit 32 - Institution Code  */
    protected $INSTITUTION_CODE = "20015";

    /** Code Bit 3 - Processong Code */
    protected $PROCESSING_CODE = array(
        "bill_inquiry"   => "380000",
        "payment_cash"   => "501000",
        "payment_cheque" => "502000",
        "payment_credit" => "503000"
    );

    /** Code Bit 41 - Card ID Bit */
    protected $CARD_ID = "101INM02";

    /** Code Bit 49 - Currency Code */
    protected $CURRENCY_CODE = "360";

    /** Code Bit 90 - Original Data Element */
    protected $ORIGINAL_DATA = "";


    /** Function To Add Zero Bit Characters As long As  */
    public function AddZeroBit($text, $length)
    {
        $temporary = '';
        for ($i=0; $i<$length - strlen($text); $i++) {
        	$temporary = $temporary.'0';
        }
        return($temporary . $text);
    }

    /** Function To Add Zero Bit  */
    public function ToAddZero($string)
    {
        $temporary = '';
        for ($i=1; $i<5-strlen($string); $i++){
            $temporary .= '0';
        }
        return($temporary . $string);
    }

    /** Function To Conver Numeric Code To String Message */
    public function GetErrorMessage($error_code)
    {
        $data = array(
            '00' => 'Transaksi Disetujui',
            '06' => 'Reversal Ditolak',
            '10' => 'Pelanggan Tidak Ditemukan',
            '13' => 'Transaksi Tidak Dapat Dilakukan',
            '14' => 'Transaksi Harus Dilakukan Di Kantor PDAM',
            '22' => 'Permintaan Saran Gagal',
            '50' => 'Jumlah Tidak Valid',
            '30' => 'Database Error',
            '86' => 'Timeout',
            '88' => 'Tagihan Sudah Terbayar',
            '89' => 'Error Link',
            '91' => 'Link Down',
            '92' => 'Potongan Tagihan (Cut off Biller)',
            '94' => 'Reversal Ditolak',
            '97' => 'Transaksi Gagal, Tagihan Sudah Dibayar',
            '98' => 'Transaksi Gagal, Respon Tagihan Tidak Terdefenisi',
            '99' => 'Error Lain Lain'
        );
        return $data[$error_code];
    }

    /** Function To Unset Data ISO */
    public function ToUnsetDataISO($data, $index)
    {
        for ($i=0; $i < count($index); $i++) {
            unset($data[$index[$i]]);
        }
        return $data;
    }

    /** Function To Get Response HTTP Curl */
    public function GetHTTPCurl($url)
    {
        $curls = curl_init();
        curl_setopt($curls, CURLOPT_URL, $url);
        curl_setopt($curls, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curls, CURLOPT_TIMEOUT, 60);
        $response_messages = curl_exec($curls);
        curl_close($curls);
        return $response_messages;
    }

    /** Function To Extract Bit 48 */
    public function ExtractionBit48PDAM($bit_msg)
    {
        $ex = array(
            "billing_id"        => trim(substr($bit_msg,0,15)),
            "customer_name"     => str_replace("  ","",substr($bit_msg, 15, 30)),
            "customer_address"  => str_replace("  ","",substr($bit_msg, 45, 30)),
            "group_rate"        => substr($bit_msg, 75, 5),
            "bill_repeat"       => intval(substr($bit_msg, 80, 2)),
            "repeat_data"       => substr($bit_msg, 82, strlen($bit_msg)),
            "data_info"         => array()
        );

        $start = 0 ;
        for ($i = 0; $i < intval($ex["bill_repeat"]); $i++)
        {
           $pdam = array(
               'bill_info'    => substr($ex["repeat_data"], $start, 1),
               'bill_periode' => substr($ex["repeat_data"], $start + 1, 6),
               'account'      => substr($ex["repeat_data"], $start + 7, 10),
               'bill_amount'  => intval(substr($ex["repeat_data"], $start + 17, 12)),
               'penalty'      => intval(substr($ex["repeat_data"], $start + 29, 8)),
               'stan_meter'	  => intval(substr($ex["repeat_data"], $start + 37, 8)),
               'usages'       => intval(substr($ex["repeat_data"], $start + 46, 8))
           );

           $start = $start + 54;
           array_push($ex["data_info"], $pdam);
        }
        return $ex;

    }

    /** Function To Get Data Array Full */
    public function GetDataArrayPDAM($bit_msg, $admin_fee)
    {
        $results = array();
        $bitlength = strlen($bit_msg);

        if($bitlength > 0)
        {
            $extract = $this->ExtractionBit48PDAM($bit_msg);
            $results["ID_PELANGGAN"]     = $extract["billing_id"];
            $results["NAMA_PELANGGAN"]   = $extract["customer_name"];
            $results["ALAMAT_PELANGGAN"] = $extract["customer_address"];
            $results["NO_TARIF"]         = $extract["group_rate"];
            $results["LEMBAR_TAGIHAN"]   = $extract["bill_repeat"];
            $results["WAKTU_TRANSAKSI"]  = date("d F Y");
            $results["BIAYA_ADMIN"]      = $admin_fee;
            $results["STRUK_INFORMASI"]  = array();

            $total = 0;
            foreach ($extract["data_info"] as $key => $value)
            {
                $tmp = array();
                $tagihan   = 0;

                foreach ($value as $k => $v)
                {
                    $tmp["INFO_TAGIHAN"]     = $value["bill_info"];
                    $tmp["PERIODE_TAGIHAN"]  = ConvertToIndonesiaFormatDate(SetMonthYear($value["bill_periode"]));
                    $tmp["BESAR_TAGIHAN"]    = intval($value["bill_amount"]);
                    $tmp["BESAR_DENDA"]      = intval($value["penalty"]);
                    $tmp["STAN_METER_AWAL"]  = (intval($value["stan_meter"]) - intval($value["usages"]));
                    $tmp["STAN_METER_AKHIR"] = intval($value["stan_meter"]);
                    $tmp["PEMAKAIAN"]        = intval($value["usages"]);
                    $tmp["JUMLAH_TAGIHAN"]   = (intval($value["bill_amount"]) + intval($value["penalty"]) + $admin_fee);
                    $tagihan = $tmp["JUMLAH_TAGIHAN"];
                }

                $total += $tagihan;
                array_push($results["STRUK_INFORMASI"], $tmp);
            }

            $results["JUMLAH_BIAYA_ADMIN"]  = $admin_fee * intval($results["LEMBAR_TAGIHAN"]);
            $results["TOTAL_BIAYA_TAGIHAN"] = $total + $admin_fee;

            return $results;
        }

        return null;
    }

    /** Convert Response HTTP Messages To ISO */
    private function ConvertResponseToISO($response)
    {
        $JAK = new JAK8583();
        $JAK->addISO(substr($response, 4, strlen($response)));
        $iso = $JAK->getData();
        return $iso;
    }

    /** Function To Process Request Inquiry PDAM */
    public function GetPDAMInquiry($customer_id, $pCode)
    {
        date_default_timezone_set("Asia/Jakarta");

        $JAK = new JAK8583();
        $INQ  = array();

        $this->AUDIT_NUMBER = mt_rand(1000, 999999);

        $JAK->addMTI("0200");
        $JAK->addData(3, $this->PROCESSING_CODE["bill_inquiry"]);
        $JAK->addData(4, "000000000000");
        $JAK->addData(7, gmdate("mdHis"));
        $JAK->addData(11, $this->AUDIT_NUMBER);
        $JAK->addData(12, date("His"));
        $JAK->addData(13, date("md"));
        $JAK->addData(15, date("md", strtotime(date("md")) + 86400));
        $JAK->addData(18, $this->MERCHANT_CODE["edc"]);
        $JAK->addData(32, $this->INSTITUTION_CODE);
        $JAK->addData(37, date("His").$this->AUDIT_NUMBER);
        $JAK->addData(41, $this->CARD_ID);
        $JAK->addData(48, $customer_id);
        $JAK->addData(49, $this->CURRENCY_CODE);
        $JAK->addData(63, $pCode);

        $INQ["messages_iso"] = $this->ToAddZero(strlen($JAK->getISO())).$JAK->getISO();
        $INQ["response_http"] = $this->GetHTTPCurl($this->IP_PORT . urlencode($INQ["messages_iso"])."&stan=".$this->AUDIT_NUMBER."&mType="."0200");

        if($INQ["response_http"] == "" || $INQ["response_http"] == false)
        {
            $INQ["error_message"]  = "Proses Time Out. Koneksi Server Terputus";
        }
        else
        {
            $JAK->addISO(substr($INQ["response_http"], 4, strlen($INQ["response_http"])));
            $INQ["data_iso"] = $JAK->getData();
        }

        return $INQ;

    }

    /** Function To Process Request Payment PDAM */
    public function GetPDAMPayment($http_inq, $pCode)
    {
        $number  = array(
            "2",  "3",  "7",  "11", "12", "15", "22", "33", "35", "37", "39", "42", "43", "59", "60", "61", "63"
        );

        $PAY = array();
        $JAK = new JAK8583();

        $dataIso = $this->ConvertResponseToISO($http_inq);
        $dataIso = $this->ToUnsetDataISO($dataIso, $number);

        $this->AUDIT_NUMBER = mt_rand(1000, 999999);

        $JAK->addMTI("0200");
        $JAK->addData(3, $this->PROCESSING_CODE["payment_cash"]);
        $JAK->addData(7, gmdate("mdHis"));
        $JAK->addData(11, $this->AUDIT_NUMBER);
        $JAK->addData(12, date("His"));
        $JAK->addData(15, date("md", strtotime(date("md")) + 86400));
        $JAK->addData(37, date("His").$this->AUDIT_NUMBER);

        foreach ($dataIso as $key => $val){
            $JAK->addData($key, $val);
        }

        $JAK->addData(63, $pCode);

        $PAY["messages_iso"]  = $this->ToAddZero(strlen($JAK->getISO())).$JAK->getISO();
        $PAY["response_http"] = $this->GetHTTPCurl($this->IP_PORT.urlencode($PAY["messages_iso"])."&stan=".$this->AUDIT_NUMBER."&mType="."0200");

        if($PAY["response_http"] == "" || $PAY["response_http"] == false)
        {
            $PAY["error_message"]  = "Proses Pembayaran PDAM Gagal. Koneksi Server Terputus";
        }
        else
        {
            $JAK->addISO(substr($PAY["response_http"], 4, strlen($PAY["response_http"])));
            $PAY["data_iso"] = $JAK->getData();
            $PAY["random_number"] = $this->AUDIT_NUMBER;
            $PAY["http_inquery"] = $http_inq;
        }

        return $PAY;

    }

    /** Function To Process Request Reversal Payment PDAM */
    public function GetPDAMReversal($http_pay, $pCode, $random)
    {
        $number   = array("3", "12", "13", "15", "37", "39", "61", "63");

        $REV = array();
        $JAK = new JAK8583();

        $dataIso = $this->ConvertResponseToISO($http_pay);
        $dataIso = $this->ToUnsetDataISO($dataIso, $number);

        $JAK->addMTI("0400");
        $JAK->addData(3, $this->PROCESSING_CODE["payment_cash"]);
        $JAK->addData(7, gmdate("mdHis"));
        $JAK->addData(11, $random);
        $JAK->addData(12, date("His"));
        $JAK->addData(13, date("md"));
        $JAK->addData(15, date("md", strtotime(date("md")) + 86400));
        $JAK->addData(37, date("His").$random);

        foreach ($dataIso as $key => $val)
        {
            if($key == 7)
                $this->TRANSMISSION_DATETIME = $val;

            if($key == 32)
                $this->DYNAMIC_CODE = $this->AddZeroBit($val, 11);

            $JAK->addData($key, $val);
        }

        $JAK->addData(63, $pCode);

        $this->ORIGINAL_DATA = "0200". $random. $this->TRANSMISSION_DATETIME. $this->DYNAMIC_CODE. $this->AddZeroBit("", 11);
        $JAK->addData(90, $this->ORIGINAL_DATA);

        $REV["messages_iso"] = $this->ToAddZero(strlen($JAK->getISO())) . $JAK->getISO();
        $REV["response_http"] = $this->GetHTTPCurl($this->IP_PORT.urlencode($REV["messages_iso"])."&stan=".$random."&mType="."0400");

        if($REV["response_http"] == "" || $REV["response_http"] == false)
        {
            $REV["error_message"] = "Reversal Time Out. Server Tidak Terkoneksi. Silahkan Hubungi PT. INM";
        }
        else
        {
            $JAK->addISO(substr($REV["response_http"], 4, strlen($REV["response_http"])));
            $REV["data_iso"] = $JAK->getData();
        }

        return $REV;

    }


}
