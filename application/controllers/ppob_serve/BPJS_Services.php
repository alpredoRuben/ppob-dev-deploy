<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class BPJS_Services extends REST_Controller
{
    protected $status = false;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('fortuna/bpjs');
    }

    public function inquiry_get($product_name, $customer_id, $numbers)
    {
        $bpjsInquiry  = array();
        $json_data = array();
        $bpjs_code = GetBPJSCode($product_name);

        $TMP = $this->bpjs->GetInquiryBPJS($customer_id, $bpjs_code, $numbers);

        $bpjsInquiry["KATEGORI_BPJS"] = GetBPJSNames($product_name);
        $bpjsInquiry["RESPONS_HTTP"]  = $TMP["respon_http"];
        $admin_fee = GetBPJSAdminFee($product_name);

        if($TMP["respon_status"] == false)
        {
            $this->status = false;
            $bpjsInquiry["PESAN_KESALAHAN"]  = "Proses Time Out. Koneksi Server Terputus";
            $bpjsInquiry["STATUS_INQUIRY"]   = $TMP["respon_status"];
        }
        else
        {
            $bpjsInquiry["STATUS_INQUIRY"] = $TMP["respon_status"];
            $bpjsInquiry["KODE_KESALAHAN"] = $TMP["data_iso"]["39"];

            if(!$this->session->userdata($product_name."_".$customer_id))
                $this->session->set_userdata($product_name."_".$customer_id, $TMP["respon_http"]);

            if($bpjsInquiry["KODE_KESALAHAN"] == "00")
            {
                $this->status = true;
                $bpjsInquiry["PESAN_KESALAHAN"] = $this->bpjs->GetErrorMessage($bpjsInquiry["KODE_KESALAHAN"]);

                //$bpjsInquiry["DATA_ISO"] = $TMP["data_iso"];
                $bpjsInquiry["DATA_EKSTRAKSI"] = $this->bpjs->GetArrayBPJSKesehatan($TMP["data_iso"]["48"], $admin_fee);

                //$json_data = $this->pdam->SetAbsoluteDataArray($inquiry);
            }
            else
            {
                $this->status = false;
                $bpjsInquiry["RESPON_HTTP"] = $TMP["respon_http"];
                $bpjsInquiry["PESAN_KESALAHAN"] = $this->bpjs->GetErrorMessage($bpjsInquiry["KODE_KESALAHAN"]);
            }

        }

        $this->response(array("STATUS"=> $this->status, "DATA_JSON" => $bpjsInquiry, "SESSION_VALUE" => $this->session->userdata($product_name."_".$customer_id)), REST_Controller::HTTP_OK);
    }

    public function payment_get($product_name, $customer_id, $numbers)
    {
        $bpjsPayment  = array();
        $http_inquiry = "";
        $TMP = null;
        $bpjs_code = GetBPJSCode($product_name);
        $admin_fee = GetBPJSAdminFee($product_name);

        if(!$this->session->userdata($product_name."_".$customer_id))
        {
            $bpjsPayment['PESAN_KESALAHAN'] = 'Proses Payment Gagal. No Pelanggan '. $customer_id . ' tidak masuk dalam cart';
        }
        else
        {
            $http_inquiry = $this->session->userdata($product_name."_".$customer_id);
            $TMP = $this->bpjs->GetPaymentBPJS($customer_id, $bpjs_code, $http_inquiry);

            if($TMP["respon_status"] == true)
            {
                $data_iso = $TMP["data_iso"];

                if($data_iso["39"] == "00")
                {
                    $respon_pay = $TMP["respon_http"];
                    $commits = $this->bpjs->GetCommitPayment($respon_pay, $bpjs_code);
                    $commits["admin_fee"] = $admin_fee;
                    $bpjsPayment["RESPONS_HTTP_PAYMENT"] = $respon_pay;
                    $bpjsPayment = $this->AuthenticatedCommit($commits, $bpjsPayment);
                    $bpjsPayment['STATUS_TRANSAKSI'] = "PAYMENT";
                }
                else
                {
                    /** Reversal Payment Condition */
                    if($data_iso["39"] == "86")
                    {
                        $reversal = $this->bpjs->GetReversalBPJS($http_inquiry, $bpjs_code);
                        if($reversal["respon_status"] == false)
                        {
                            $this->status = false;
                            $bpjsPayment['PESAN_KESALAHAN'] = $reversal["pesan_error"];
                            $bpjsPayment['RESPONS_HTTP_PAYMENT'] = $reversal["respon_http"];
                        }
                        else
                        {
                            $respon_rev = $reversal["respon_http"];
                            $rev_commit = $this->bpjs->GetCommitPayment($respon_pay, $bpjs_code);
                            $rev_commit["admin_fee"] = $admin_fee;
                            $bpjsPayment["RESPONS_HTTP_PAYMENT"] = $respon_pay;
                            $bpjsPayment = $this->AuthenticatedCommit($rev_commit, $bpjsPayment);
                        }

                        $bpjsPayment['STATUS_TRANSAKSI'] = "REVERSAL";
                    }
                }
            }
            else
            {
                $this->status = false;
                $bpjsPayment["RESPONS_HTTP"] = $TMP["respon_http"];
                $bpjsPayment["PESAN_KESALAHAN"] = $TMP["pesan_error"];
            }
        }

        $this->response(array("STATUS"=> $this->status, "DATA_JSON" => $bpjsPayment), REST_Controller::HTTP_OK);
    }

    /** Function To Commit Process Response Payment */
    public function AuthenticatedCommit($commit, $data)
    {
        if($commit["respon_status"] == true)
        {
            $com_iso = $commit["data_iso"];

            if($com_iso["39"] == "00")
            {
                $this->status = true;
                $ex_commit = $this->bpjs->GetArrayBPJSKesehatan($com_iso["48"], $commit["admin_fee"]);
                $data["KODE_KESALAHAN"]       = $com_iso["39"];
                $data["PESAN_KESALAHAN"]      = $this->bpjs->GetErrorMessage($data["KODE_KESALAHAN"]);
                $data["BIT_PESAN_ISO_COMMIT"] = $commit["pesan_iso"];
                $data["RESPONSE_HTTP_COMMIT"] = $commit["respon_http"];
                $data["DATA_EKSTRAKSI"]       = $ex_commit;
            }
            else
            {
                $this->status = false;
                $data["RESPONSE_HTTP_COMMIT"] = $commit["respon_http"];
                $data["KODE_KESALAHAN"]       = $com_iso["39"];
                $data["PESAN_KESALAHAN"]      = $this->bpjs->GetErrorMessage($data["KODE_KESALAHAN"]);
            }
        }
        else
        {
            $this->status = false;
            $data["PESAN_KESALAHAN"] = $commit["pesan_error"];
            $data["RESPONSE_HTTP_COMMIT"] = $commit["respon_http"];
        }

        return $data;
    }

}
