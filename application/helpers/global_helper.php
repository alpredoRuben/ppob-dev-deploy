<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** Function To Format Month and Year */
if(!function_exists('SetMonthYear'))
{
    function SetMonthYear($_str_date)
    {
        $date = new DateTime();

        $_month = intval(substr($_str_date,4,2));
        $_year  = (substr($_str_date,0,4));

        $date->setDate($_year, $_month, 3);
        return $date->format('Y-m');
    }
}

/** Format DateTime Indonesia */
if(!function_exists('ConvertToIndonesiaFormatDate'))
{
    function ConvertToIndonesiaFormatDate($format){
    	$bulan = array (
    		1 =>  'Januari', 'Februari', 'Maret', 'April','Mei',
    		      'Juni', 'Juli', 'Agustus', 'September',
                  'Oktober','Nopember','Desember'
    	);

        $tmp = explode('-', $format);
    	return $bulan[(int) $tmp[1]] . ' ' . $tmp[0];
    }

}

/** Function To Explode String Message To Array Character */
if(!function_exists('ToExplodeStringToChar'))
{
    function ToExplodeStringToChar($str)
    {
        $results = array();
        for ($i=0; $i < strlen($str); $i++) {
            $results[$i] = "Karakter[".$i."]    = " . $str[$i];
        }

        return $results;
    }
}
