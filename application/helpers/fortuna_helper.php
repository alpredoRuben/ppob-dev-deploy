<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** ALL METHOD BPJS PRODUCT */
if(!function_exists('GetBPJSNames'))
{
    function GetBPJSNames($name = '')
    {
        $data = array(
            'BPJS_KS' => 'BPJS Kesehatan',
            'BPJS_TK' => 'BPJS Ketenagakerjaan'
        );
        return ($name == '') ? $data : $data[$name];
    }
}

if(!function_exists('GetBPJSCode'))
{
    function GetBPJSCode($names = '')
    {
        $data = array(
            'BPJS_KS' => '4000',
            'BPJS_TK' => '4001'
        );
        return ($names != '')? $data[$names] : $data;
    }
}

if(!function_exists('GetBPJSAdminFee'))
{
    function GetBPJSAdminFee($names = '')
    {
        $data = array(
            'BPJS_KS' => 2500,
            'BPJS_TK' => 2500
        );
        return ($names != '')? $data[$names] : $data;
    }
}
/** END ALL METHOD BPJS PRODUCT */

/** ALL METHOD PDAM INISIAL PRODUCT */
if(!function_exists('GetPDAMCode'))
{
    function GetPDAMCode($names = '')
    {
        $data = array(
            'AETRA_AIR_JAKARTA'             => '1003',
            'AETRA_AIR_TANGERANG'	   		=> '',
            'PDAM_KOTA_MEDAN'          		=> '1002',
            'PDAM_KOTA_SIANTAR'             => '1039',
        	'PDAM_KOTA_TEBING_TINGGI'  		=> '1037',
        	'PDAM_KOTA_TANGERANG'      		=> '1001',
            'PDAM_KOTA_DENPASAR' 	   		=> '',
            'PDAM_KOTA_BEKASI' 		   		=> '',
            'PDAM_KOTA_BANDUNG'				=> '',
        	'PDAM_KOTA_BOGOR'				=> '',
        	'PDAM_KOTA_CIREBON'				=> '',
            'PDAM_KOTA_DEPOK'				=> '',
            'PDAM_KOTA_PEKALONGAN' 			=> '',
            'PDAM_KOTA_MADIUN' 				=> '',
            'PDAM_KOTA_SURAKARTA' 			=> '',
            'PDAM_KOTA_MALANG' 				=> '',
            'PDAM_KOTA_PONTIANAK' 			=> '',
            'PDAM_KOTA_MAGELANG' 			=> '',
            'PDAM_KOTA_SOLOK'               => '1053',
            'PDAM_KAB_MAGELANG' 			=> '',
        	'PDAM_KAB_KARANGANYAR' 		    => '',
        	'PDAM_KAB_WONOSOBO' 			=> '',
        	'PDAM_KAB_BANYUMAS' 			=> '',
            'PDAM_KAB_TANGERANG'  	   		=> '1000',
        	'PDAM_KAB_KARAWANG'		   		=> '',
        	'PDAM_KAB_BANDUNG'		   		=> '1136',
        	'PDAM_KAB_GARUT'		   		=> '1135',
        	'PDAM_KAB_BOGOR' 		   		=> '',
        	'PDAM_KAB_BEKASI' 				=> '',
        	'PDAM_KAB_INDRAMAYU' 	   		=> '',
        	'PDAM_KAB_CIANJUR' 		   		=> '',
        	'PDAM_KAB_TASIKMALAYA' 	   		=> '',
        	'PDAM_KAB_KUNINGAN'				=> '',
        	'PDAM_KAB_SERANG'				=> '1115',
        	'PDAM_KAB_RANGKAS_BITUNG'		=> '',
        	'PDAM_KAB_CIAMIS' 				=> '',
            'PDAM_KAB_BATANG' 				=> '',
        	'PDAM_KAB_DEMAK' 				=> '',
        	'PDAM_KAB_GROBOGAN' 			=> '',
        	'PDAM_KAB_BOYOLALI' 			=> '',
        	'PDAM_KAB_SUKOHARJO' 			=> '',
            'PDAM_KAB_SIDOARJO' 			=> '',
            'PDAM_KAB_MAGETAN' 				=> '',
            'PDAM_KAB_BANYUWANGI' 			=> '',
            'PDAM_KAB_MALANG' 				=> '',
        	'PDAM_KAB_GRESIK' 				=> '',
        	'PDAM_KAB_TEGAL' 				=> '',
        	'PDAM_KAB_LAMPUNG'				=> '',
        	'PDAM_KAB_BANJARMASIN' 			=> '',
            'PDAM_KAB_KENDAL' 				=> '',
        	'PDAM_KAB_KLATEN' 				=> '',
            'PDAM_KAB_NIAS'                 => '1046',
        	'IPL_PAM_SINARMASLAND' 			=> '',
        	'IPL_PAM_SUMMARECON_SERPONG' 	=> '',
        	'IPL_PAM_PARAMOUNT_SERPONG' 	=> '',
        	'IPL_PAM_ALAM_SUTERA' 			=> '',
        	'IPL_PAM_CITRA_RAYA_TANGERANG' 	=> '',
            'IPL_PAM_LIPPO_KAWARACI' 		=> '',
        	'IPL_PAM_BINTARO' 				=> '',
            'PDAM_BANTUL' 					=> '',
        	'PDAM_KULONPROGO' 				=> '',
        	'PDAM_KAB_SLEMAN' 				=> '',
        	'PDAM_KAB_GUNUNG_KIDUL' 		=> '',
        	'PDAM_KOTA_MATARAM' 			=> '',
            'PDAM_KOTA_MANADO'              => '1022'
        );
        return ($names != '')? $data[$names] : $data;
    }
}

if(!function_exists('GetPDAMAdminFee'))
{
    function GetPDAMAdminFee($names = '')
    {
        $data = array(
            'AETRA_AIR_JAKARTA'             => 2500,
            'AETRA_AIR_TANGERANG'	   		=> 2500,
            'PDAM_KOTA_MEDAN'          		=> 2500,
            'PDAM_KOTA_SIANTAR'             => 2500,
            'PDAM_KOTA_TEBING_TINGGI'  		=> 2500,
            'PDAM_KOTA_TANGERANG'      		=> 2500,
            'PDAM_KOTA_DENPASAR' 	   		=> 2500,
            'PDAM_KOTA_BEKASI' 		   		=> 2500,
            'PDAM_KOTA_BANDUNG'				=> 2500,
            'PDAM_KOTA_BOGOR'				=> 2500,
            'PDAM_KOTA_CIREBON'				=> 2500,
            'PDAM_KOTA_DEPOK'				=> 2500,
            'PDAM_KOTA_PEKALONGAN' 			=> 2500,
            'PDAM_KOTA_MADIUN' 				=> 2500,
            'PDAM_KOTA_SURAKARTA' 			=> 2500,
            'PDAM_KOTA_MALANG' 				=> 2500,
            'PDAM_KOTA_PONTIANAK' 			=> 2500,
            'PDAM_KOTA_MAGELANG' 			=> 2500,
            'PDAM_KOTA_SOLOK'               => 2500,
            'PDAM_KOTA_MATARAM' 			=> 2500,
            'PDAM_KOTA_MANADO'              => 2500,
            'PDAM_KAB_MAGELANG' 			=> 2500,
            'PDAM_KAB_KARANGANYAR' 		    => 2500,
            'PDAM_KAB_WONOSOBO' 			=> 2500,
            'PDAM_KAB_BANYUMAS' 			=> 2500,
            'PDAM_KAB_TANGERANG'  	   		=> 2500,
            'PDAM_KAB_KARAWANG'		   		=> 2500,
            'PDAM_KAB_BANDUNG'		   		=> 2500,
            'PDAM_KAB_GARUT'		   		=> 3000,
            'PDAM_KAB_BOGOR' 		   		=> 2500,
            'PDAM_KAB_BEKASI' 				=> 2500,
            'PDAM_KAB_INDRAMAYU' 	   		=> 2500,
            'PDAM_KAB_CIANJUR' 		   		=> 2500,
            'PDAM_KAB_TASIKMALAYA' 	   		=> 2500,
            'PDAM_KAB_KUNINGAN'				=> 2500,
            'PDAM_KAB_SERANG'				=> 2500,
            'PDAM_KAB_RANGKAS_BITUNG'		=> 2500,
            'PDAM_KAB_CIAMIS' 				=> 2500,
            'PDAM_KAB_BATANG' 				=> 2500,
            'PDAM_KAB_DEMAK' 				=> 2500,
            'PDAM_KAB_GROBOGAN' 			=> 2500,
            'PDAM_KAB_BOYOLALI' 			=> 2500,
            'PDAM_KAB_SUKOHARJO' 			=> 2500,
            'PDAM_KAB_SIDOARJO' 			=> 2500,
            'PDAM_KAB_MAGETAN' 				=> 2500,
            'PDAM_KAB_BANYUWANGI' 			=> 2500,
            'PDAM_KAB_MALANG' 				=> 2500,
            'PDAM_KAB_GRESIK' 				=> 2500,
            'PDAM_KAB_TEGAL' 				=> 2500,
            'PDAM_KAB_LAMPUNG'				=> 2500,
            'PDAM_KAB_BANJARMASIN' 			=> 2500,
            'PDAM_KAB_KENDAL' 				=> 2500,
            'PDAM_KAB_KLATEN' 				=> 2500,
            'PDAM_KAB_NIAS'                 => 2500,
            'PDAM_KAB_SLEMAN' 				=> 2500,
            'PDAM_KAB_GUNUNG_KIDUL' 	    => 2500,
            'IPL_PAM_SINARMASLAND' 			=> 2500,
            'IPL_PAM_SUMMARECON_SERPONG' 	=> 2500,
            'IPL_PAM_PARAMOUNT_SERPONG' 	=> 2500,
            'IPL_PAM_ALAM_SUTERA' 			=> 2500,
            'IPL_PAM_CITRA_RAYA_TANGERANG' 	=> 2500,
            'IPL_PAM_LIPPO_KAWARACI' 		=> 2500,
            'IPL_PAM_BINTARO' 				=> 2500,
            'PDAM_BANTUL' 					=> 2500,
            'PDAM_KULONPROGO' 				=> 2500,
        );
        return ($names != '')? $data[$names] : $data;
    }
}

if(!function_exists('GetPDAMNames'))
{
    function GetPDAMNames($name = '')
    {
        $data = array(
            'AETRA_AIR_JAKARTA'             => 'PT. Aetra Air Jakarta',
            'AETRA_AIR_TANGERANG'	   		=> 'PT. Aetra Air Tangerang',
            'PDAM_KOTA_MEDAN'          		=> 'PDAM Tirtanadi Kota Medan',
            'PDAM_KOTA_SIANTAR'             => 'PDAM Tirtauli Kota Pematang Siantar',
            'PDAM_KOTA_TEBING_TINGGI'  		=> 'PDAM Tirta Bulian Kota Tebing Tinggi',
            'PDAM_KOTA_TANGERANG'      		=> 'PDAM Tirta Benteng Kota Tangerang',
            'PDAM_KOTA_DENPASAR' 	   		=> 'PDAM Kota Denpasar',
            'PDAM_KOTA_BEKASI' 		   		=> 'PDAM Tirta Patriot Kota Bekasi',
            'PDAM_KOTA_BANDUNG'				=> 'PDAM Tirtawening Kota Bandung',
            'PDAM_KOTA_BOGOR'				=> 'PDAM Tirta Pakuan Kota Bogor',
            'PDAM_KOTA_CIREBON'				=> 'PERUMDA Tirta Giri Nata Kota Cirebon',
            'PDAM_KOTA_DEPOK'				=> 'PDAM Tirta Asasta Kota Depok',
            'PDAM_KOTA_PEKALONGAN' 			=> 'PDAM Kota Pekalongan',
            'PDAM_KOTA_MADIUN' 				=> 'PDAM Tirta Taman Sari Kota Madiun',
            'PDAM_KOTA_SURAKARTA' 			=> 'PDAM Kota Surakarta (Pusat)',
            'PDAM_KOTA_MALANG' 				=> 'PDAM Tirta Dharma Kota Malang',
            'PDAM_KOTA_PONTIANAK' 			=> 'PDAM Tirta Khatulistiwa Kota Pontianak',
            'PDAM_KOTA_MAGELANG' 			=> 'PDAM Kota Magelang',
            'PDAM_KOTA_SOLOK'               => 'PDAM Kota Solok',
            'PDAM_KOTA_MATARAM' 			=> 'PDAM Giri Menang Kota Mataram',
            'PDAM_KOTA_MANADO'              => 'PDAM Tikala Kota Manado',
            'PDAM_KAB_MAGELANG' 			=> 'PDAM Tirta Gemilang Kabupaten Magelang',
            'PDAM_KAB_KARANGANYAR' 		    => 'PDAM Tirta Lawu Kabupaten Karanganyar',
            'PDAM_KAB_WONOSOBO' 			=> 'PDAM Tirta Aju Kabupaten Wonosobo',
            'PDAM_KAB_BANYUMAS' 			=> 'PDAM Tirta Satria Kabupaten Banyumas',
            'PDAM_KAB_TANGERANG'  	   		=> 'PDAM Tirta Kerta Raharja Kabupaten Tangerang',
            'PDAM_KAB_KARAWANG'		   		=> 'PDAM Tirta Tarum Kabupaten Karawang',
            'PDAM_KAB_BANDUNG'		   		=> 'PDAM Tirta Raharja Kabupaten Bandung',
            'PDAM_KAB_GARUT'		   		=> 'PDAM Tirta Intan Kabupaten Garut',
            'PDAM_KAB_BOGOR' 		   		=> 'PDAM Tirta Kahuripan Kabupaten Bogor',
            'PDAM_KAB_BEKASI' 				=> 'PDAM Tirta Bhagasasi Bekasi Kabupaten Bekasi',
            'PDAM_KAB_INDRAMAYU' 	   		=> 'PDAM Tirta Darma Ayu Kabupaten Indramayu',
            'PDAM_KAB_CIANJUR' 		   		=> 'PDAM Tirta Mukti Kabupaten Cianjur',
            'PDAM_KAB_TASIKMALAYA' 	   		=> 'PDAM Tirta Sukapura Kabupaten Tasikmalaya',
            'PDAM_KAB_KUNINGAN'				=> 'PDAM Tirta Kamuning Kabupaten Kuningan',
            'PDAM_KAB_SERANG'				=> 'PERPAMSI Kabupaten Serang',
            'PDAM_KAB_RANGKAS_BITUNG'		=> 'PDAM Tirta Multatuli Kabupaten Lebak (Rangkasbitung)',
            'PDAM_KAB_CIAMIS' 				=> 'PDAM Tirta Galuh Kabupaten Ciamis',
            'PDAM_KAB_BATANG' 				=> 'PDAM Kabupaten Batang',
            'PDAM_KAB_DEMAK' 				=> 'PDAM Kabupaten Demak',
            'PDAM_KAB_GROBOGAN' 			=> 'PDAM Purwa Tirta Dharma Kabupaten Grobogan',
            'PDAM_KAB_BOYOLALI' 			=> 'PUDAM Tirta Ampera Kabupaten Boyolali',
            'PDAM_KAB_SUKOHARJO' 			=> 'PDAM Tirta Makmur Kabupaten Sukoharjo',
            'PDAM_KAB_SIDOARJO' 			=> 'PDAM Delta Tirta Kabupaten Sidoarjo',
            'PDAM_KAB_MAGETAN' 				=> 'PDAM Kabupaten Magetan',
            'PDAM_KAB_BANYUWANGI' 			=> 'PDAM Kabupaten Banyuwangi',
            'PDAM_KAB_MALANG' 				=> 'PDAM Kabupaten Malang',
            'PDAM_KAB_GRESIK' 				=> 'PDAM Giri Tirta Kabupaten Gresik',
            'PDAM_KAB_TEGAL' 				=> 'PDAM Kabupaten Tegal',
            'PDAM_KAB_LAMPUNG'				=> 'PDAM Tirta Jasa Kabupaten Lampung Selatan',
            'PDAM_KAB_BANJARMASIN' 			=> 'PDAM Intan Banjar Kabupaten Banjarmasin',
            'PDAM_KAB_KENDAL' 				=> 'PDAM Tirto Panguripan Kabupaten Kendal',
            'PDAM_KAB_KLATEN' 				=> 'PDAM Kabupaten Klaten',
            'PDAM_KAB_NIAS'                 => 'PDAM Tirta Umbu Kabupaten Nias',
            'PDAM_KAB_SLEMAN' 				=> 'PDAM Kabupaten Sleman',
            'PDAM_KAB_GUNUNG_KIDUL' 	    => 'PDAM Tirta Handayani Kabupaten Gunung Kidul',
            'IPL_PAM_SINARMASLAND' 			=> 'IPL PAM Sinarmasland',
            'IPL_PAM_SUMMARECON_SERPONG' 	=> 'IPL PAM Summarecon Serpong',
            'IPL_PAM_PARAMOUNT_SERPONG' 	=> 'IPL PAM Paramount Serpong',
            'IPL_PAM_ALAM_SUTERA' 			=> 'IPL PAM Alam Sutera',
            'IPL_PAM_CITRA_RAYA_TANGERANG' 	=> 'IPL PAM Citra Raya Tangerang',
            'IPL_PAM_LIPPO_KAWARACI' 		=> 'IPL PAM Lippo Kawaraci',
            'IPL_PAM_BINTARO' 				=> 'IPL PAM Bintaro',
            'PDAM_BANTUL' 					=> 'PDAM Bantul',
            'PDAM_KULONPROGO' 				=> 'PDAM Tirta Binangun Kulonprogo'
        );
        return ($name == '') ? $data : $data[$name];
    }
}

if(!function_exists('GetPDAMProvince'))
{
    function GetPDAMProvince($name)
    {
        $province = "";
        switch ($name)
        {
            case 'IPL_PAM_SINARMASLAND'         :
            case 'IPL_PAM_SUMMARECON_SERPONG'   :
            case 'IPL_PAM_PARAMOUNT_SERPONG'    :
            case 'IPL_PAM_ALAM_SUTERA'          :
            case 'IPL_PAM_CITRA_RAYA_TANGERANG' :
            case 'IPL_PAM_LIPPO_KAWARACI'       :
                $province = '';
            break;

            case 'IPL_PAM_BINTARO'              :
            case 'AETRA_AIR_JAKARTA'            :
                $province = 'Provinsi DKI Jakarta';
            break;


            case 'PDAM_KAB_SLEMAN'       :
            case 'PDAM_KAB_GUNUNG_KIDUL' :
            case 'PDAM_BANTUL'           :
            case 'PDAM_KULONPROGO'       :
                $province = 'Provinsi Daerah Istimewa Yogyakarta';
            break;

            case 'PDAM_KAB_BANJARMASIN'  :
                $province = 'Provinsi Kalimantan Selatan';
            break;

            case 'PDAM_KOTA_PONTIANAK'   :
                $province = 'Provinsi Kalimantan Barat';
            break;

            case 'PDAM_KAB_LAMPUNG'     :
                $province = 'Provinsi Lampung';
            break;

            case 'PDAM_KOTA_MANADO'     :
                $province = 'Provinsi Sulawesi Utara';
            break;

            case 'PDAM_KOTA_MATARAM'    :
                $province = 'Provinsi Nusa Tenggara Barat';
            break;

            case 'PDAM_KOTA_MADIUN'     :
            case 'PDAM_KOTA_MALANG'     :
            case 'PDAM_KAB_SIDOARJO'    :
            case 'PDAM_KAB_MAGETAN'     :
            case 'PDAM_KAB_BANYUWANGI'  :
            case 'PDAM_KAB_MALANG'      :
            case 'PDAM_KAB_GRESIK'      :
                $province = 'Provinsi Jawa Timur';
            break;

            case 'PDAM_KOTA_PEKALONGAN' :
            case 'PDAM_KOTA_SURAKARTA'  :
            case 'PDAM_KOTA_MAGELANG'   :
            case 'PDAM_KAB_MAGELANG'    :
            case 'PDAM_KAB_KARANGANYAR':
            case 'PDAM_KAB_WONOSOBO'    :
            case 'PDAM_KAB_BANYUMAS'    :
            case 'PDAM_KAB_BATANG'      :
            case 'PDAM_KAB_DEMAK'       :
            case 'PDAM_KAB_GROBOGAN'    :
            case 'PDAM_KAB_BOYOLALI'    :
            case 'PDAM_KAB_SUKOHARJO'   :
            case 'PDAM_KAB_TEGAL'       :
            case 'PDAM_KAB_KENDAL'      :
            case 'PDAM_KAB_KLATEN'      :
                $province = 'Provinsi Jawa Tengah';
            break;

            case 'PDAM_KOTA_BEKASI'     :
            case 'PDAM_KOTA_BANDUNG'    :
            case 'PDAM_KOTA_BOGOR'      :
            case 'PDAM_KOTA_CIREBON'    :
            case 'PDAM_KOTA_DEPOK'      :
            case 'PDAM_KAB_KARAWANG'    :
            case 'PDAM_KAB_BANDUNG'     :
            case 'PDAM_KAB_GARUT'       :
            case 'PDAM_KAB_BOGOR'       :
            case 'PDAM_KAB_BEKASI'      :
            case 'PDAM_KAB_INDRAMAYU'   :
            case 'PDAM_KAB_CIANJUR'     :
            case 'PDAM_KAB_TASIKMALAYA' :
            case 'PDAM_KAB_KUNINGAN'    :
            case 'PDAM_KAB_CIAMIS'      :
                $province = 'Provinsi Jawa Barat';
            break;

            case 'PDAM_KOTA_DENPASAR'   :
                $province = 'Provinsi Bali';
            break;

            case 'AETRA_AIR_TANGERANG'     :
            case 'PDAM_KOTA_TANGERANG'     :
            case 'PDAM_KAB_TANGERANG'      :
            case 'PDAM_KAB_SERANG'         :
            case 'PDAM_KAB_RANGKAS_BITUNG' :
                $province = 'Provinsi Banten';
            break;

            case 'PDAM_KOTA_MEDAN'         :
            case 'PDAM_KOTA_SIANTAR'       :
            case 'PDAM_KOTA_TEBING_TINGGI' :
            case 'PDAM_KAB_NIAS'           :
                $province = 'Provinsi Sumatera Utara';
            break;

            case 'PDAM_KOTA_SOLOK'     :
                $province = 'Provinsi Sumatera Barat';
            break;
        }

        return $province;
    }
}
/** END ALL METHOD PDAM INISIAL PRODUCT */
