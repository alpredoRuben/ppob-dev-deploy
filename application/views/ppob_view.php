<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PPOB PDAM</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css">


</head>

<body>

<div class="container" style="margin-top:40px;">

    <div class="row">
        <div class="col-sm-12">
            <div id="headContent">
                <form id="formCheckInquiry" class="form-inline">
                    <div class="form-group">
                        <label for="nama_produk">Produk</label>
                        <select id="nama_produk" name="nama_produk" class="form-control">
                            <option value="-">--- PILIH PRODUK ---</option>
                        <?php foreach ($inisial_produk as $key => $value) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    &nbsp;
                    <div class="form-group">
                        <label for="no_pelanggan">No Pelanggan</label>
                        <input type="text" class="form-control" id="no_pelanggan" name="no_pelanggan" placeholder="No Pelanggan">
                    </div>
                    &nbsp;
                    <div class="form-group">
                        <button type="submit" id="btn_check" class="btn btn-primary">
                            Check Pelanggan
                        </button>
                        &nbsp;
                        <button type="submit" id="btn_bayar" class="btn btn-success">
                            Bayar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="contents">
                <div id="kertas">
                </div>
            </div>
        </div>
    </div>

</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
<script src="<?php echo site_url('assets/templates/js/web.js'); ?>"></script>


</body>
</html>
