<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH. "/libraries/JAK8583/JAK8583.php";

class Bpjs
{
    //protected $ip_port   = "http://localhost:8080?q="; //IP & Port Production
    protected $ip_port   = "http://localhost:4101?q=";   //IP & Port Development
    protected $perBitLength  = 134;

    protected $MERCHANT_CODE = array(
        "teller"      => "6010",
        "atm"         => "6011",
        "edc"         => "6012",
        "auto_debit"  => "6013",
        "e_banking"   => "6014",
        "kiosk"       => "6015",
        "phone_bank"  => "6016",
        "mobile_bank" => "6017"
    );

    protected $INSTITUTION_CODE = "20015";

    protected $PROCESSING_CODE = array(
        "bill_inquiry"   => "380000",
        "payment_cash"   => "501000",
        "commit_payment" => "502000"
    );

    protected $CARD_ID = "101INM02";

    /** Function To Add Zero Left */
    public function ToAddZero($string)
    {
        $rs='';

    	for ($i=1; $i<5-strlen($string); $i++){
    		$rs = $rs. '0';
    	}

    	return($rs.$string);
    }

    function AddZeroBit($text,$ln)
    {
    	$rs='';
    	for ($i=0; $i<$ln-strlen($text); $i++)
    	{
    		$rs=$rs.'0';
    	}
    	return($rs.$text);
    }


    /** Function To Get Response HTTP Curl */
    public function GetHTTPCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /** Function To Unset Data ISO */
    public function ToUnsetDataISO($data, $index)
    {
        for ($i=0; $i < count($index); $i++) {
            unset($data[$index[$i]]);
        }
        return $data;
    }

    /** Function To Conver Numeric Code Error To String Message */
    public function GetErrorMessage($code_error)
    {
        $data = array(
            '00' => 'Transaksi Disetujui',
            '06' => 'Reversal Ditolak. Transaksi Asli Tidak Ditemukan',
            '10' => 'Pelanggan Tidak Ditemukan',
            '13' => 'Transaksi Tidak Dapat Dilakukan',
            '14' => 'Transaksi Harus Dilakukan Di Kantor PDAM',
            '30' => 'Database Error',
            '50' => 'Jumlah Tidak Valid',
            '86' => 'Timeout',
            '88' => 'Tagihan Sudah Terbayar',
            '89' => 'Error Link',
            '94' => 'Reversal Ditolak, Transaksi Sudah Dibatalkan',
            '99' => 'Error Lain Lain'
        );
        return $data[$code_error];
    }

    /** Extraction Bit 48 Message Response To Array  */
    public function ExtractBitBPJSKesehatan($bit)
    {
        $ex  = array();
        $tmp = array();
        $bitlen = strlen($bit);

        $ex = array(
            "billing_id"  => trim(substr($bit, 0, 16)),   // Billing ID, String Customer ID [0, 16]
            "month_bill"  => intval(substr($bit, 16, 2)), // Month Bill, Integer Number Month Bill [16, 2]
            "nokapst"     => substr($bit, 18, 13),        // NoKAPST, String Number KAPST [18, 3]
            "bill_repeat" => intval(substr($bit, 31, 2)), // Repeat Count, Integer Bill Repeat Count Data Available [31,2]
            "repeat_data" => substr($bit, 33, $bitlen),   // Repeat Data, String Repeat Bit Message Data [33, Bit Message Length]
            "commit_number" => null, //To Commit as Reference Number
            "data_info"   => array()
        );

        $repeatDataLength   = strlen($ex["repeat_data"]);  // Repeat Data Length
        $selisih = strlen($ex["repeat_data"]) - ($ex["bill_repeat"] * 134);
        $data_loop = "";

        if($repeatDataLength % 134 == 0)
        {
            $data_loop = $ex["repeat_data"];
            $ex["number_refference"] = "";
        }
        else
        {
            $data_loop = substr($ex["repeat_data"], 0, strlen($ex["repeat_data"]) - $selisih);
            $ex["number_refference"] = substr($ex["repeat_data"], $ex["bill_repeat"] * 134, $selisih);
        }


        $ex["repeatLength"] = $this->perBitLength;

        $index = 0;
        for ($i=0; $i < $ex["bill_repeat"]; $i++)
        {
            $tmp[$i] = substr($data_loop, $index, $this->perBitLength);
            $index += $this->perBitLength;
        }

    	for ($i = 0; $i < $ex["bill_repeat"]; $i++)
        {
            $bpjs = array(
                "CUSTOMER_ID"     => substr($tmp[$i],   0, 16),
                "CUSTOMER_NAME"   => trim(substr($tmp[$i],  16, 35)),
                "PREMI_ID"        => trim(substr($tmp[$i],  51,  8)),
                "BRANCH_ID"       => trim(substr($tmp[$i],  59,  4)),
                "BRANCH_N"        => trim(substr($tmp[$i],  63, 35)),
                "PREMI_DIMUKA"    => intval(substr($tmp[$i], -36, 12)),
                "PREMI_BULAN_INI" => intval(substr($tmp[$i], -24, 12)),
                "BILL_AMOUNT"     => intval(substr($tmp[$i], -12))
            );

            array_push($ex["data_info"], $bpjs);
    	}

        return $ex;
    }

    /** Get and Control Array Data Request Inquiry */
    public function GetArrayBPJSKesehatan($bit, $admin_fee)
    {
        $results = array();
        $bitlength = strlen($bit);
        if($bitlength > 0)
        {
            $extraction = $this->ExtractBitBPJSKesehatan($bit);
            $results["NO_REFERENSI"]            = $extraction["number_refference"];
            $results["NO_BILL_PELANGGAN"]       = $extraction["billing_id"];
            $results["NO_AKUN_VIRTUAL"]         = $extraction["data_info"][0]["CUSTOMER_ID"];
            $results["BULAN_TAGIHAN"]           = $extraction["month_bill"];
            $results["NOKAPST"]                 = $extraction["nokapst"];
            $results["TOTAL_PESERTA_BPJS"]      = $extraction["bill_repeat"];
            $results["REPEAT_DATA"]             = $extraction["repeat_data"];
            $results["WAKTU_REQUEST_INQUIRY"]   = date("d F Y , H:i:s");
            $results["BIAYA_ADMIN"]             = $admin_fee;
            $results["NAMA_PENDAFTAR_BPJS"]     = $extraction["data_info"][0]["CUSTOMER_NAME"];
            $results["STRUK_INFORMASI"] = array();

            $total = 0;
            foreach ($extraction["data_info"] as $key => $value)
            {
                $tmp = array();
                $total_tagihan = 0;
                foreach ($value as $k => $v)
                {
                    $tmp["ID_PELANGGAN"]          = $value["CUSTOMER_ID"];
                    $tmp["NAMA_PELANGGAN"]        = $value["CUSTOMER_NAME"];
                    $tmp["ID_PREMI"]              = $value["PREMI_ID"];
                    $tmp["ID_BRANCH"]             = $value["BRANCH_ID"];
                    $tmp["NAMA_BRANCH"]           = $value["BRANCH_N"];
                    $tmp["BIAYA_PREMI_DIMUKA"]    = $value["PREMI_DIMUKA"];
                    $tmp["BIAYA_PREMI_BULAN_INI"] = $value["PREMI_BULAN_INI"];
                    $tmp["BIAYA_TAGIHAN"]         = $value["BILL_AMOUNT"];

                    $total_tagihan = $tmp["BIAYA_TAGIHAN"];
                }

                $total += $total_tagihan;
                array_push($results["STRUK_INFORMASI"], $tmp);
            }

            $results["JUMLAH_BIAYA_TAGIHAN"]  = $total;
            $results["TOTAL_SELURUH_TAGIHAN"] = $total + $admin_fee;

            return $results;
        }
        else
            return null;

    }


    /* -------------------------------------INQUIRY AND PAYMENT METHOD ------------------------------------ */

    //GetInquiryBPJS($customer_id, $bpjs_code, $numbers)
    public function GetInquiryBPJS($customer_id, $bpjs_code, $numbers)
    {
        date_default_timezone_set('Asia/Jakarta');
        //require APPPATH. "/libraries/JAK8583/JAK8583.php";

        if(intval($numbers) < 10)
            $numbers = '0'.$numbers;

        $jakBPJS   = new JAK8583();
        $bpjsinq = array();

        //add data
        $jakBPJS->addMTI('0200');
        $jakBPJS->addData(3, $this->PROCESSING_CODE["bill_inquiry"]);
        $jakBPJS->addData(4, '000000000000');
        $jakBPJS->addData(7, gmdate("mdHis"));

        $random = mt_rand(1000, 999999);

        $jakBPJS->addData(11, $random);
        $jakBPJS->addData(12, date("His"));
        $jakBPJS->addData(13, date("md"));
        $jakBPJS->addData(15, date('md', strtotime(date('md')) + 86400));
        $jakBPJS->addData(18, $this->MERCHANT_CODE["edc"]);
        $jakBPJS->addData(32, $this->INSTITUTION_CODE);
        $jakBPJS->addData(37, date("His").$random);
        $jakBPJS->addData(41, $this->CARD_ID);
        $jakBPJS->addData(48, $customer_id . $numbers);
        $jakBPJS->addData(49, '360');
        $jakBPJS->addData(63, $bpjs_code);

        $bpjsinq["pesan_iso"]   = $this->ToAddZero(strlen($jakBPJS->getISO())).$jakBPJS->getISO();
        $bpjsinq["respon_http"] = $this->GetHTTPCurl($this->ip_port . urlencode($bpjsinq["pesan_iso"])."&stan=".$random."&mType=".'0200');

        if($bpjsinq["respon_http"] == "" || $bpjsinq["respon_http"] == false)
            $bpjsinq["respon_status"] = false;
        else
        {
            $jakBPJS->addISO(substr($bpjsinq["respon_http"], 4, strlen($bpjsinq["respon_http"])));
            $bpjsinq["data_iso"] = $jakBPJS->getData();
            $bpjsinq["respon_status"] = true;
        }

        return $bpjsinq;

    }

    /** Function To Process Request Payment PDAM*/
    public function GetPaymentBPJS($customer_id, $bpjs_code, $http_inquiry)
    {
        date_default_timezone_set('Asia/Jakarta');
        //require APPPATH. "/libraries/JAK8583/JAK8583.php";

        $number  = array("3","7","11","12","15","37","39","61","63");
        $bpjspay = array();

        $jak = new JAK8583();
        $pay = new JAK8583();

        $jak->addISO(substr($http_inquiry, 4, strlen($http_inquiry)));
        $random = mt_rand(1000, 999999);

        $data = $jak->getData();
        $data = $this->ToUnsetDataISO($data, $number);

        $pay->addMTI('0200');
    	$pay->addData(3, $this->PROCESSING_CODE["payment_cash"]);
        $pay->addData(7, gmdate("mdHis"));
    	$pay->addData(11, $random);
    	$pay->addData(12, date("His"));
    	$pay->addData(15, date('md', strtotime(date('md')) + 86400));
    	$pay->addData(37, date("His").$random);
    	foreach ($data as $key => $val) {
    		$pay->addData($key, $val);
    	}
    	$pay->addData(63, $bpjs_code);

        $bpjspay["pesan_iso"] = $this->ToAddZero(strlen($pay->getISO())).$pay->getISO();
        $bpjspay["respon_http"] = $this->GetHTTPCurl($this->ip_port.urlencode($bpjspay["pesan_iso"])."&stan=".$random."&mType=".'0200');

        if($bpjspay["respon_http"] == "" || $bpjspay["respon_http"] == false)
        {
            $bpjspay["respon_status"] = false;
            $bpjspay["pesan_error"] = "Transaksi Pembayaran BPJS Dengan ID Pelanggan ". $customer_id . " Gagal";
        }
        else
        {
            $pay->addISO(substr($bpjspay["respon_http"], 4, strlen($bpjspay["respon_http"])));
            $bpjspay["respon_status"] = true;
	        $bpjspay["data_iso"] = $pay->getData();
            $bpjspay["random"] = $random;
        }

        return $bpjspay;
    }

    /** Function To Process Request Reversal Payment **/
    public function GetReversalBPJS($http_inquiry, $bpjs_code)
    {
        $number   = array("3", "7", "11", "12", "15", "37", "39", "61", "63");
        $bpjsrev = array();

        $jak = new JAK8583();
        $pay = new JAK8583();

        $jak->addISO(substr($http_inquiry, 4, strlen($http_inquiry)));
        $data = $jak->getData();
        $data = $this->ToUnsetDataISO($data, $number);

        $random = mt_rand(1000, 999999);
        $pay->addMTI('0200');
    	$pay->addData(3, $this->PROCESSING_CODE["payment_cash"]);
    	$pay->addData(7, gmdate("mdHis"));
    	$pay->addData(11, $random);
    	$pay->addData(12, date("His"));
    	$pay->addData(15, date('md', strtotime(date('md')) + 86400));
    	$pay->addData(37, date("His").$random);
    	foreach ($data as $key => $val){
    		$pay->addData($key, $val);
    	}
    	$pay->addData(63, $bpjs_code);

        $bpjsrev["pesan_iso"] = $this->ToAddZero(strlen($pay->getISO())).$pay->getISO();
        $bpjsrev["respon_http"] = $this->GetHTTPCurl($this->ip_port.urlencode($bpjsrev["pesan_iso"])."&stan=".$random."&mType=".'0200');

        if($bpjsrev["respon_http"] =='' || $bpjsrev["respon_http"] == false)
        {
            $bpjsrev["respon_status"] = false;
            $bpjsrev["pesan_error"] = "Transaksi Pembayaran Gagal";
        }
        else
        {
            $pay->addISO(substr($bpjsrev["respon_http"], 4, strlen($bpjsrev["respon_http"])));
            $bpjsrev["respon_status"]  = true;
	        $bpjsrev["data_iso"]       = $pay->getData();
        }

        return $bpjsrev;
    }

    /** Function To Process Request Reversal Payment **/
    public function GetCommitPayment($respon_payment, $bpjs_code)
    {
        $number   = array("3", "7", "11", "12", "15", "37", "39","63");

        $bpjscommit = array();
        $jak = new JAK8583();
        $comm = new JAK8583();

        $jak->addISO(substr($respon_payment, 4, strlen($respon_payment)));
        $data = $jak->getData();
        $data = $this->ToUnsetDataISO($data, $number);

        $random = mt_rand(1000, 999999);
        $bit7  = '';
        $bit32 = '';

        $comm->addMTI('0200');
        $comm->addData(3, $this->PROCESSING_CODE["commit_payment"]);
        $comm->addData(7, gmdate("mdHis"));
        $comm->addData(11, $random);
        $comm->addData(12, date("His"));
        $comm->addData(15, date('md', strtotime(date('md')) + 86400));
        $comm->addData(37, date("His").$random);

        foreach ($data as $key => $val)
        {
            if($key==7)
		        $bit7 = $val;

            if($key==32)
		        $bit32 = $this->AddZeroBit($val,11);

            $comm->addData($key, $val);
        }

        $comm->addData(63, $bpjs_code);
        $bit90 = "0200". $random. $bit7. $bit32. "00000000000";
        $comm->addData(90, $bit90);

        $bpjscommit["pesan_iso"] = $this->ToAddZero(strlen($comm->getISO())).$comm->getISO();
        $bpjscommit["respon_http"] = $this->GetHTTPCurl($this->ip_port.urlencode($bpjscommit["pesan_iso"])."&stan=".$random."&mType=".'0200');

        if($bpjscommit["respon_http"] == "" || $bpjscommit["respon_http"] == false)
        {
            $bpjscommit["respon_status"] = false;
            $bpjscommit["pesan_error"] = "Transaksi Pembayaran Gagal";
        }
        else
        {
            $comm->addISO(substr($bpjscommit["respon_http"], 4, strlen($bpjscommit["respon_http"])));
            $bpjscommit["respon_status"] = true;
	        $bpjscommit["data_iso"] = $comm->getData();
            $bpjscommit["random"] = $random;
        }

        return $bpjscommit;
    }






















}
