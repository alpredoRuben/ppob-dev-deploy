# REST SERVER API BPJS

## 1. Inisial, Kode dan Nama Produk

Susunan kompleksitas konstanta variable produk, terdiri atas:

- kode produk
- nama inisial (define unique names)
- nama produk

Daftar konstanta variabel produk BPJS

Kode Produk | Nama Inisial | Nama Produk
----------- | ------------ | --------------------
4000        | BPJS_KS      | BPJS Kesehatan
4001        | BPJS_TK      | BPJS Ketenagakerjaan

## 2. Ketetapan

- Panjang karakter No Virtual Account BPJS Kesehatan (ID Pelanggan) yang diinput adalah 16 digit karakter angka (Sesuai no yang tertera di kartu BPJS Kesehatan pelanggan tersebut)
- Untuk Kategori BPJS Kesehatan Pilihan Keluarga, nomor Virtual Account (VA) yang ditampilkan adalah nomor Virtual Account (VA) yang mendaftarkan berdasarkan data dari BPJS Kesehatan.

## 3. Root Service Name

### a. Rest Service URL

METHOD | URL SERVICE / ROOT LINK                    | NAMA PROSES | DATA                                       | NAMA PRODUK    | SESSION ACTIVE
------ | ------------------------------------------ | ----------- | ------------------------------------------ | -------------- | ----------------------------
GET    | rest/get/bpjs/inquiry/(:any)/(:any)/(:any) | Inquiry     | {nama_produk, id_pelanggan, periode_bayar} | BPJS Kesehatan | {nama_produk}_{id_pelanggan}
GET    | rest/get/bpjs/payment/(:any)/(:any)/(:any) | Payment     | {nama_produk, id_pelanggan, periode_bayar} | BPJS Kesehatan | [Checking Session Actice]

### b. Contoh Penggunaan Rest Server API Service Inquiry BPJS Kesehatan

```json
{
    "method"    : "get",
    "routes"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/bpjs/inquiry/(:any)/(:any)/(:any)",
    "category"  : "BPJS Kesehatan",
    "contoh"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/bpjs/inquiry/BPJS_KS/8888801284375969/{2}[dinamis]"
}
```

### c. Contoh Penggunaan Rest Server API Service Payment BPJS Kesehatan

```json
{
    "method"    : "get",
    "routes"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/bpjs/payment/(:any)/(:any)/(:any)",
    "category"  : "BPJS Kesehatan",
    "contoh"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/bpjs/payment/BPJS_KS/8888801284375969/{2}[dinamis]",
    "checked"   : "session inquiry"
}
```

## 4. Respon Data Messages

Setiap request dalam rest api ini akan mengembalikan data dengan format _respon messages data_ : "JSON".

### a. Contoh Proses _Request Inquiry_

> Request URL Inquiry : http://pay-inm.co.id/inm_lab/ppob/rest/get/bpjs/payment/BPJS_KS/8888801835275792/3
> Kategori VA : _BPJS Kesehatan Virtual Account Keluarga_

Data JSON Request Inquiry :

```json
{
  "STATUS": true,
  "DATA_JSON": {
    "KATEGORI_BPJS": "BPJS Kesehatan",
    "RESPONS_HTTP":
      "05870210323A40010A8180063800000000001530000109111146334867181146010901106012052001518114633486700101INM024358888801835275792030000000000000038888801835273755BENI FEBRIANTO                     123363690401PEKANBARU                          0000000510000000000000000000000510008888801835275792FARIDAH aNUM                       123365690401PEKANBARU                          0000000510000000000000000000000510008888801835278413SYUA NADIF FEBRIAN                 123367870401PEKANBARU                          000000051000000000000000000000051000360035TEST FOOTER DEVELOPMENT BPJSKS GEN20044000",
    "STATUS_INQUIRY": true,
    "KODE_KESALAHAN": "00",
    "PESAN_KESALAHAN": "Transaksi Disetujui",
    "DATA_EKSTRAKSI": {
      "NO_REFERENSI": "8888801835275792000000000051000",
      "NO_BILL_PELANGGAN": "8888801835275792",
      "NO_AKUN_VIRTUAL": "8888801835273755",
      "BULAN_TAGIHAN": 3,
      "NOKAPST": "0000000000000",
      "TOTAL_PESERTA_BPJS": 3,
      "REPEAT_DATA":
        "8888801835273755BENI FEBRIANTO                     123363690401PEKANBARU                          0000000510000000000000000000000510008888801835275792FARIDAH aNUM                       123365690401PEKANBARU                          0000000510000000000000000000000510008888801835278413SYUA NADIF FEBRIAN                 123367870401PEKANBARU                          000000051000000000000000000000051000",
      "WAKTU_REQUEST_INQUIRY": "09 January 2018 , 18:11:46",
      "BIAYA_ADMIN": 2500,
      "NAMA_PENDAFTAR_BPJS": "BENI FEBRIANTO",
      "STRUK_INFORMASI": [
        {
          "ID_PELANGGAN": "8888801835273755",
          "NAMA_PELANGGAN": "BENI FEBRIANTO",
          "ID_PREMI": "12336369",
          "ID_BRANCH": "0401",
          "NAMA_BRANCH": "PEKANBARU",
          "BIAYA_PREMI_DIMUKA": 51000,
          "BIAYA_PREMI_BULAN_INI": 0,
          "BIAYA_TAGIHAN": 51000
        },
        {
          "ID_PELANGGAN": "8888801835275792",
          "NAMA_PELANGGAN": "FARIDAH aNUM",
          "ID_PREMI": "12336569",
          "ID_BRANCH": "0401",
          "NAMA_BRANCH": "PEKANBARU",
          "BIAYA_PREMI_DIMUKA": 51000,
          "BIAYA_PREMI_BULAN_INI": 0,
          "BIAYA_TAGIHAN": 51000
        },
        {
          "ID_PELANGGAN": "8888801835278413",
          "NAMA_PELANGGAN": "SYUA NADIF FEBRIAN",
          "ID_PREMI": "12336787",
          "ID_BRANCH": "0401",
          "NAMA_BRANCH": "PEKANBARU",
          "BIAYA_PREMI_DIMUKA": 51000,
          "BIAYA_PREMI_BULAN_INI": 0,
          "BIAYA_TAGIHAN": 51000
        }
      ],
      "JUMLAH_BIAYA_TAGIHAN": 153000,
      "TOTAL_SELURUH_TAGIHAN": 155500
    }
  },
  "SESSION_VALUE":
    "05870210323A40010A8180063800000000000765000109094604673978164604010901106012052001516460467397800101INM024358888801835275792010000000000000038888801835273755BENI FEBRIANTO                     123363690401PEKANBARU                          0000000000000000000255000000000255008888801835275792FARIDAH aNUM                       123365690401PEKANBARU                          0000000000000000000255000000000255008888801835278413SYUA NADIF FEBRIAN                 123367870401PEKANBARU                          000000000000000000025500000000025500360035TEST FOOTER DEVELOPMENT BPJSKS GEN20044000"
}
```

### b. Contoh Proses _Request Payment With Commit_**

> Request URL Payment : http://pay-inm.co.id/inm_lab/ppob/rest/get/bpjs/payment/BPJS_KS/8888801835275792/3
> Kategori VA : _BPJS Kesehatan Virtual Account Keluarga_ Keterangan : *Kondisi nomor Virtual Account (ID Pelanggan) dan Priode bulan yang di pilih sesuai saat request inquiry"


Data JSON Request Payment With Commit Saat _Terjadi Error Request [Status Koneksi Payment di tutup]_:

```json
{
  "STATUS": false,
  "DATA_JSON": {
    "RESPONS_HTTP_PAYMENT":
      "06210210323A40010A8380065010000000000765000109111749446083181749010901106012052001518174944608300101INM020168008EDA39A5AF1804508888801835275792010000000000000038888801835273755BENI FEBRIANTO                     123363690401PEKANBARU                          0000000000000000000255000000000255008888801835275792FARIDAH aNUM                       123365690401PEKANBARU                          0000000000000000000255000000000255008888801835278413SYUA NADIF FEBRIAN                 123367870401PEKANBARU                          000000000000000000025500000000025500FM4460831817491360035TEST FOOTER DEVELOPMENT BPJSKS GEN20044000",
    "RESPONSE_HTTP_COMMIT":
      "06790210B23A40010A83800600000040000000005020000000000000000109111749180213181749010901106012052001518174918021399101INM020168008EDA39A5AF1804508888801835275792010000000000000038888801835273755BENI FEBRIANTO                     123363690401PEKANBARU                          0000000000000000000255000000000255008888801835275792FARIDAH aNUM                       123365690401PEKANBARU                          0000000000000000000255000000000255008888801835278413SYUA NADIF FEBRIAN                 123367870401PEKANBARU                          000000000000000000025500000000025500FM4460831817491360035TEST FOOTER DEVELOPMENT BPJSKS GEN20044000          02001802130000002001500000000000",
    "KODE_KESALAHAN": "99",
    "PESAN_KESALAHAN": "Error Lain Lain",
    "STATUS_TRANSAKSI": "PAYMENT"
  }
}
```

# REST SERVER API PDAM

## 1. Inisial, Kode dan Nama Produk


| KODE PRODUK | NAMA INISIAL                    | NAMA PRODUK                          |
|-------------|---------------------------------|--------------------------------------|
| 1003        | AETRA_AIR_JAKARTA               | PT. Aetra Air Jakarta                |
|             | AETRA_AIR_TANGERANG 	   	    | PT. Aetra Air Tangerang              |
| 1002        | PDAM_KOTA_MEDAN           	    | PDAM Tirtanadi Kota Medan            |
| 1039        | PDAM_KOTA_SIANTAR               | PDAM Tirtauli Kota Pematang Siantar  |
| 1037        | PDAM_KOTA_TEBING_TINGGI   	    | PDAM Tirta Bulian Kota Tebing Tinggi |
| 1001        | PDAM_KOTA_TANGERANG       	    | PDAM Tirta Benteng Kota Tangerang    |
|             | PDAM_KOTA_DENPASAR  	   	    | PDAM Kota Denpasar                   |
|             | PDAM_KOTA_BEKASI  		   	    | PDAM Tirta Patriot Kota Bekasi       |
|             | PDAM_KOTA_BANDUNG 			    | PDAM Tirtawening Kota Bandung        |
|             | PDAM_KOTA_BOGOR 			    | PDAM Tirta Pakuan Kota Bogor         |
|             | PDAM_KOTA_CIREBON 			    | PERUMDA Tirta Giri Nata Kota Cirebon |
|             | PDAM_KOTA_DEPOK 			    | PDAM Tirta Asasta Kota Depok         |
|             | PDAM_KOTA_PEKALONGAN  		    | PDAM Kota Pekalongan                 |
|             | PDAM_KOTA_MADIUN  			    | PDAM Tirta Taman Sari Kota Madiun    |
|             | PDAM_KOTA_SURAKARTA  		    | PDAM Kota Surakarta (Pusat)          |
|             | PDAM_KOTA_MALANG  			    | PDAM Tirta Dharma Kota Malang        |
|             | PDAM_KOTA_PONTIANAK  		    | PDAM Tirta Khatulistiwa Kota Pontianak |
|             | PDAM_KOTA_MAGELANG  		    | PDAM Kota Magelang                   |
| 1053        | PDAM_KOTA_SOLOK                 | PDAM Kota Solok                      |
|             | PDAM_KOTA_MATARAM 			    | PDAM Giri Menang Kota Mataram        |
| 1022        | PDAM_KOTA_MANADO                | PDAM Tikala Kota Manado              |
|             | PDAM_KAB_MAGELANG  			    | PDAM Tirta Gemilang Kabupaten Magelang |
|             | PDAM_KAB_KARANGANYAR  		    | PDAM Tirta Lawu Kabupaten Karanganyar |
|             | PDAM_KAB_WONOSOBO  			    | PDAM Tirta Aju Kabupaten Wonosobo     |
|             | PDAM_KAB_BANYUMAS  			    | PDAM Tirta Satria Kabupaten Banyumas  |
| 1000        | PDAM_KAB_TANGERANG   	   	    | PDAM Tirta Kerta Raharja Kabupaten Tangerang |
|             | PDAM_KAB_KARAWANG 		   		| PDAM Tirta Tarum Kabupaten Karawang |
| 1136        | PDAM_KAB_BANDUNG 		   		| PDAM Tirta Raharja Kabupaten Bandung |
| 1135        | PDAM_KAB_GARUT 		   		    | PDAM Tirta Intan Kabupaten Garut |
|             | PDAM_KAB_BOGOR  		   		| PDAM Tirta Kahuripan Kabupaten Bogor |
|             | PDAM_KAB_BEKASI  				| PDAM Tirta Bhagasasi Bekasi Kabupaten Bekasi |
|             | PDAM_KAB_INDRAMAYU  	   		| PDAM Tirta Darma Ayu Kabupaten Indramayu |
|             | PDAM_KAB_CIANJUR  		   		| PDAM Tirta Mukti Kabupaten Cianjur |
|             | PDAM_KAB_TASIKMALAYA  	   		| PDAM Tirta Sukapura Kabupaten Tasikmalaya |
|             | PDAM_KAB_KUNINGAN 				| PDAM Tirta Kamuning Kabupaten Kuningan |
| 1115        | PDAM_KAB_SERANG 				| PERPAMSI Kabupaten Serang ,
|             | PDAM_KAB_RANGKAS_BITUNG 		| PDAM Tirta Multatuli Kabupaten Lebak (Rangkasbitung) |
|             | PDAM_KAB_CIAMIS  				| PDAM Tirta Galuh Kabupaten Ciamis |
|             | PDAM_KAB_BATANG  				| PDAM Kabupaten Batang |
|             | PDAM_KAB_DEMAK  				| PDAM Kabupaten Demak |
|             | PDAM_KAB_GROBOGAN  			    | PDAM Purwa Tirta Dharma Kabupaten Grobogan |
|             | PDAM_KAB_BOYOLALI  			    | PUDAM Tirta Ampera Kabupaten Boyolali |
|             | PDAM_KAB_SUKOHARJO  			| PDAM Tirta Makmur Kabupaten Sukoharjo |
|             | PDAM_KAB_SIDOARJO  			    | PDAM Delta Tirta Kabupaten Sidoarjo |
|             | PDAM_KAB_MAGETAN  				| PDAM Kabupaten Magetan |
|             | PDAM_KAB_BANYUWANGI  			| PDAM Kabupaten Banyuwangi |
|             | PDAM_KAB_MALANG  				| PDAM Kabupaten Malang |
|             | PDAM_KAB_GRESIK  				| PDAM Giri Tirta Kabupaten Gresik |
|             | PDAM_KAB_TEGAL  				| PDAM Kabupaten Tegal |
|             | PDAM_KAB_LAMPUNG 				| PDAM Tirta Jasa Kabupaten Lampung Selatan |
|             | PDAM_KAB_BANJARMASIN  			| PDAM Intan Banjar Kabupaten Banjarmasin |
|             | PDAM_KAB_KENDAL  				| PDAM Tirto Panguripan Kabupaten Kendal |
|             | PDAM_KAB_KLATEN  				| PDAM Kabupaten Klaten |
| 1046        | PDAM_KAB_NIAS                   | PDAM Tirta Umbu Kabupaten Nias |


## 2. Ketetapan

> Jumlah karakter No Pelanggan (ID Pelanggan) PDAM berbeda masing-masing kota/kabupaten

## 3. Root Service Name

### a. Rest Service URL

METHOD | URL SERVICE / ROOT LINK             | NAMA PROSES | DATA                        | NAMA PRODUK | SESSION ACTIVE
------ | ----------------------------------- | ----------- | --------------------------- | ----------- | ----------------------------
GET    | rest/get/pdam/inquiry/(:any)/(:any) | Inquiry     | {nama_produk, id_pelanggan} | PDAM        | {nama_produk}_{id_pelanggan}
GET    | rest/get/pdam/payment/(:any)/(:any) | Payment     | {nama_produk, id_pelanggan} | PDAM        | [Checking Session Actice]

> Note: Setiap Request Inquiry, Secara Otomatis No Pelanggan Tersimpan Ke Dalam Session Berdasarkan Produk Pilihan

### b. Contoh Penggunaan Rest Server API Service Inquiry PDAM

```json
{
    "method"    : "get",
    "routes"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/pdam/inquiry/(:any)/(:any)",
    "category"  : "PDAM Tirtauli Medan",
    "contoh"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/pdam/inquiry/PDAM_MEDAN/1109810004"
}
```

### c. Contoh Penggunaan Rest Server API Service Payment PDAM

```json
{
    "method"    : "get",
    "routes"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/pdam/payment/(:any)/(:any)",
    "category"  : "PDAM Tirtauli Medan",
    "contoh"    : "http://pay-inm.co.id/inm_lab/ppob/rest/get/pdam/payment/PDAM_MEDAN/1109810004",
    "checked"   : "session inquiry"
}
```

## 4. Respon Data Messages

Setiap request dalam rest api *PDAM* ini akan mengembalikan data dengan format _respon messages data_ : "JSON".

### a. Contoh Proses _Request Inquiry_ Berhasil (Disetujui)

> Request URL Inquiry : http://pay-inm.co.id/inm_lab/ppob/rest/get/pdam/inquiry/PDAM_KOTA_MEDAN/0113460007
> Kategori Produk: _PDAM TIRTANADI MEDAN_
> No. Pelanggan : 0113460007

Data JSON Request Inquiry :

```json

{
    "STATUS": true,
    "RESULTS": {
        "NAMA_PDAM": "PDAM Tirtanadi Kota Medan",
        "NAMA_PROVINSI": "Provinsi Sumatera Utara",
        "RESPON_HTTP_INQUIRY": "04710210323A40010A81800A3800000000874950000116075812819715145812011601176012052001514581281971500101INM023520113460007     NURHANI                       SISINGAMANGARAJA Gg SUPIR NO.8RT.3 0512017080000000000000182950.000001000003095000-0004500022017090000000000000160500.000001000003135000-0004000032017100000000000000164990.000001000003176000-0004100042017110000000000000160500.000001000003216000-0004000052017120000000000000156010.000001000003255000-00039000360002000041002",
        "PESAN_BIT_ISO": "01220200323A40010881800238000000000000000001160758128197151458120116011760120520015145812819715101INM0201001134600073600041002",
        "KODE_KESALAHAN": "00",
        "PESAN_KESALAHAN": "Transaksi Disetujui",
        "DATA_EKSTRAKSI": {
            "ID_PELANGGAN": "0113460007",
            "NAMA_PELANGGAN": "NURHANI ",
            "ALAMAT_PELANGGAN": "SISINGAMANGARAJA Gg SUPIR NO.8",
            "NO_TARIF": "RT.3 ",
            "LEMBAR_TAGIHAN": 5,
            "WAKTU_TRANSAKSI": "16 January 2018",
            "BIAYA_ADMIN": 2500,
            "STRUK_INFORMASI": [
                {
                    "INFO_TAGIHAN": "1",
                    "PERIODE_TAGIHAN": "Agustus 2017",
                    "BESAR_TAGIHAN": 182950,
                    "BESAR_DENDA": 10000,
                    "STAN_METER_AWAL": 3050000,
                    "STAN_METER_AKHIR": 3095000,
                    "PEMAKAIAN": 45000,
                    "JUMLAH_TAGIHAN": 195450
                },
                {
                    "INFO_TAGIHAN": "2",
                    "PERIODE_TAGIHAN": "September 2017",
                    "BESAR_TAGIHAN": 160500,
                    "BESAR_DENDA": 10000,
                    "STAN_METER_AWAL": 3095000,
                    "STAN_METER_AKHIR": 3135000,
                    "PEMAKAIAN": 40000,
                    "JUMLAH_TAGIHAN": 173000
                },
                {
                    "INFO_TAGIHAN": "3",
                    "PERIODE_TAGIHAN": "Oktober 2017",
                    "BESAR_TAGIHAN": 164990,
                    "BESAR_DENDA": 10000,
                    "STAN_METER_AWAL": 3135000,
                    "STAN_METER_AKHIR": 3176000,
                    "PEMAKAIAN": 41000,
                    "JUMLAH_TAGIHAN": 177490
                },
                {
                    "INFO_TAGIHAN": "4",
                    "PERIODE_TAGIHAN": "Nopember 2017",
                    "BESAR_TAGIHAN": 160500,
                    "BESAR_DENDA": 10000,
                    "STAN_METER_AWAL": 3176000,
                    "STAN_METER_AKHIR": 3216000,
                    "PEMAKAIAN": 40000,
                    "JUMLAH_TAGIHAN": 173000
                },
                {
                    "INFO_TAGIHAN": "5",
                    "PERIODE_TAGIHAN": "Desember 2017",
                    "BESAR_TAGIHAN": 156010,
                    "BESAR_DENDA": 10000,
                    "STAN_METER_AWAL": 3216000,
                    "STAN_METER_AKHIR": 3255000,
                    "PEMAKAIAN": 39000,
                    "JUMLAH_TAGIHAN": 168510
                }
            ],
            "JUMLAH_BIAYA_ADMIN": 12500,
            "TOTAL_BIAYA_TAGIHAN": 889950
        }
    },
    "SESSION": "04710210323A40010A81800A3800000000874950000116075812819715145812011601176012052001514581281971500101INM023520113460007     NURHANI                       SISINGAMANGARAJA Gg SUPIR NO.8RT.3 0512017080000000000000182950.000001000003095000-0004500022017090000000000000160500.000001000003135000-0004000032017100000000000000164990.000001000003176000-0004100042017110000000000000160500.000001000003216000-0004000052017120000000000000156010.000001000003255000-00039000360002000041002"
}

```


### b. Contoh Proses _Request Inquiry_ Berhasil (Tidak Disetujui)

> Request URL Inquiry : http://pay-inm.co.id/inm_lab/ppob/rest/get/pdam/inquiry/PDAM_KOTA_MEDAN/0106200015
> Kategori Produk: _PDAM TIRTANADI MEDAN_
> No. Pelanggan : 0106200015
> Kesalahan : Transaksi Tidak Disetujui

Data JSON Request Inquiry :

```json
{
    "STATUS": false,
    "RESULTS": {
        "NAMA_PDAM": "PDAM Tirtanadi Kota Medan",
        "NAMA_PROVINSI": "Provinsi Sumatera Utara",
        "RESPON_HTTP_INQUIRY": "01290210323A40010A81800A3800000000000000000116082840680515152840011601176012052001515284068051513101INM020100106200015360002050041002",
        "PESAN_BIT_ISO": "01220200323A40010881800238000000000000000001160828406805151528400116011760120520015152840680515101INM0201001062000153600041002",
        "KODE_KESALAHAN": "13",
        "PESAN_KESALAHAN": "Transaksi Tidak Dapat Dilakukan"
    },
    "OPTIONAL": "01290210323A40010A81800A3800000000000000000116082840680515152840011601176012052001515284068051513101INM020100106200015360002050041002"
}
```
