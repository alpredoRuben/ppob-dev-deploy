'use strict'

var inputNoPelanggan = $("input[name=no_pelanggan]");
var inputNamaProduk  = $("select[name=nama_produk]");

var canvasViewers    = $('#canvas');
var tombolCheck      = $("#btn_check");
var tombolBayar      = $('#btn_bayar');

$(document).ready(function() {
    tombolCheck.on('click', (e) => {
        e.preventDefault();

        var urls = window.base_url + "pdam/inquiry";

        window.cart = {
            no_pelanggan : inputNoPelanggan.val(),
            nama_produk  : inputNamaProduk.val()
        };

        //console.log(window.cart);

        axios.post(urls, window.cart).then(function (response) {
            console.log("Inquiry Process");
            console.log(response);

            // var data_json = response.data.json_inquiry;
            // var status    = response.data.status;

            // if(status == false)
            // {
            //     if(data_json.respon_http == false)
            //         ToGetDialogBox("Request Time Out", data_json.pesan_error);
            //     else
            //         ToGetDialogBox("Transaksi PDAM Gagal - ["+ data_json.kode_error +"]", "Maaf, " + data_json.pesan_error);
            // }
            // else
            // {
            //     var ekstraksi   = data_json.ekstraksi_bit;
            //     var struk_info  = ekstraksi.struk_info;
            //     var lembar      = parseInt(struk_info.length);
            //
            //     ToGetDialogBox(data_json.pesan_error.toUpperCase(), "Perminataan Pencarian No. Pelanggan " + ekstraksi.no_pelanggan + " Berhasil");
            //     SetPDAMDataJSONToCanvas(canvasViewers, data_json, ekstraksi, lembar, struk_info);
            // }


        }).catch(function (error) {
            console.log(error);
        });

    })

    tombolBayar.on('click', function(events){
        event.preventDefault();
        var urls = window.base_url + 'pdam/payment';

        axios.post(urls, window.cart).then(function (response) {
            console.log("Payment Process");
            console.log(response);
            // var data_pay = response.data.json_payment;
            //
            // if(status == false)
            // {
            //     if(data_pay.respon_http == false || data_pay.respon_http == '')
            //         ToGetDialogBox("Request Time Out", data_pay.pesan_error);
            //     else
            //     {
            //         ToGetDialogBox("Reversal Payment - ["+ data_pay.kode_error +"]",  data_pay.pesan_error);
            //         SetCanvasJSONPDAMPayment(canvasViewers, data_pay)
            //     }
            // }
            // else
            // {
            //     ToGetDialogBox("Success Payment - ["+ data_pay.kode_error +"]", data_pay.pesan_error);
            //     SetCanvasJSONPDAMPayment(canvasViewers, data_pay);
            // }

        }).catch(function (error) {
            console.log(error);
        });

    });

});
