<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class PDAM extends REST_Controller
{
    /** Instanced To Set Response Status Event */
    private $EVENT_STATUS = false;

    /** Instanced To Set Session Value */
    private $OPTIONAL_SESSION = "";

    /** Instanced To Set Response HTTP {Inquiry, Payment, Reversal Or Committed} */
    private $RESPONSE_MESSAGES = "";

    /** Instance To Set Product Code */
    private $PRODUCT_CODE = "";

    /** Instance To Set Product Name */
    private $PRODUCT_NAME = "";

    /** Instance To Set Fee From Admin Bank  */
    private $ADMIN_BANK_FEE = "";


    /** Constructor */
    public function __construct()
    {
        parent::__construct();
        $this->load->library("fortuna/pdamlib");
    }

    /** Function To Check Session */
    public function CheckSession($session_names)
    {
        if($this->session->userdata($session_names))
            return true;
        else
            return false;
    }

    /** Function To Get Data Session as Session Names */
    public function GetSession($session_names)
    {
        $session = $this->session->userdata($session_names);
        return $session;
    }

    /** Function To Create Session Event */
    public function CreateSession($session_names, $data)
    {
        $this->session->set_userdata($session_names, $data);
    }

    /** Function To Delete Session as Session Names */
    public function UnsetSession($session_names)
    {
        $this->session->unset_userdata($session_names);
    }

    /** Function To Set And Return Response Value With Args */
    public function ApiResponse($status, $json, $optional=null, $method)
    {
        $data = array("STATUS" => $status, "RESULTS" => $json, "OPTIONAL" => $optional);

        switch ($method) {
            case 404:
                return $this->response($data, REST_Controller::HTTP_NOT_FOUND);
                break;
            case 200:
                return $this->response($data, REST_Controller::HTTP_OK);
                break;
            default:
                return $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
                break;
        }
    }

    /** Rest Server Api Serve Request Inquiry PDAM */
    public function Inquiry_get($inisial, $customer_id)
    {
        $pdamInquiry = array();

        $session_name = $inisial."_".$customer_id;

        $this->ADMIN_BANK_FEE = GetPDAMAdminFee($inisial);
        $this->PRODUCT_CODE = GetPDAMCode($inisial);
        $this->PRODUCT_NAME = GetPDAMNames($inisial);

        $INQ = $this->pdamlib->GetPDAMInquiry($customer_id, $this->PRODUCT_CODE);

        $this->RESPONSE_MESSAGES = $INQ["response_http"];

        $pdamInquiry["NAMA_PDAM"] = $this->PRODUCT_NAME;
        $pdamInquiry["NAMA_PROVINSI"] = GetPDAMProvince($inisial);
        $pdamInquiry["RESPON_HTTP_INQUIRY"] = $this->RESPONSE_MESSAGES;

        if($this->RESPONSE_MESSAGES == "" || $this->RESPONSE_MESSAGES == false)
        {
            $this->EVENT_STATUS = false;
            $pdamInquiry["PESAN_KESALAHAN"] = $INQ["error_message"];
        }
        else
        {
            $data_iso  = $INQ["data_iso"];
            $error_msg = $data_iso["39"];

            $pdamInquiry["DATA_ISO"] = $INQ["data_iso"];
            $pdamInquiry["PESAN_BIT_ISO"] = $INQ["messages_iso"];
            $pdamInquiry["KODE_KESALAHAN"]  = $error_msg;

            if($this->CheckSession($session_name))
                $this->UnsetSession($session_name);

            $this->CreateSession($session_name, $INQ["response_http"]);
            $this->OPTIONAL_SESSION = $this->GetSession($session_name);

            if($error_msg == "00")
            {
                $this->EVENT_STATUS = true;
                $pdamInquiry["PESAN_KESALAHAN"] = $this->pdamlib->GetErrorMessage($error_msg);
                $pdamInquiry["DATA_EKSTRAKSI"]  = $this->pdamlib->GetDataArrayPDAM($data_iso["48"], $this->ADMIN_BANK_FEE);
            }
            else
            {
                $this->EVENT_STATUS = false;
                $pdamInquiry["PESAN_KESALAHAN"] = $this->pdamlib->GetErrorMessage($error_msg);
            }
        }

        $this->ApiResponse($this->EVENT_STATUS, $pdamInquiry, $this->OPTIONAL_SESSION, 200);
    }

    /** Rest Server Api Serve Request Payment PDAM */
    public function Payment_get($inisial, $customer_id)
    {
        $pdamPayment = array();

        $session_name = $inisial."_".$customer_id;

        $this->ADMIN_BANK_FEE = GetPDAMAdminFee($inisial);
        $this->PRODUCT_CODE = GetPDAMCode($inisial);
        $this->PRODUCT_NAME = GetPDAMNames($inisial);

        if($this->CheckSession($session_name) == false)
        {
            $pdamPayment["PESAN_KESALAHAN"] = "Transaksi Pembayaran Gagal. No Pelanggan ". $customer_id . " Tidak Masuk Dalam Cart";
        }
        else
        {
            $this->OPTIONAL_SESSION = $this->GetSession($session_name);

            $PAY  = $this->pdamlib->GetPDAMPayment($this->OPTIONAL_SESSION, $this->PRODUCT_CODE);

            $pdamPayment["MESSAGE_ISO"] = $PAY["messages_iso"];
            $pdamPayment["RESPON_HTTP_PAYMENT"] = $PAY["response_http"];
            $pdamPayment["STATUS_TRANSAKSI"] = "PAYMENT";

            $this->RESPONSE_MESSAGES = $PAY["response_http"];
            if($this->RESPONSE_MESSAGES == "" || $this->RESPONSE_MESSAGES == false)
            {
                $this->EVENT_STATUS = false;
                $pdamPayment["PESAN_KESALAHAN"] = $PAY["error_message"];
            }
            else
            {
                $data_iso  = $PAY["data_iso"];
                $error_msg = $data_iso["39"];

                $pdamPayment["PESAN_BIT_ISO"]  = $PAY["messages_iso"];
                $pdamPayment["KODE_KESALAHAN"] = $error_msg;

                if($error_msg == "00")
                {
                    $this->EVENT_STATUS = true;
                    $pdamPayment["PESAN_KESALAHAN"] = $this->pdamlib->GetErrorMessage($error_msg);
                    $pdamPayment["DATA_EKSTRAKSI"] = $this->pdamlib->GetDataArrayPDAM($data_iso["48"], $this->ADMIN_BANK_FEE);
                }
                else
                {
                    if($error_msg == "86")
                    {
                        $pdamPayment = $this->PDAMReversalPayment($PAY["http_inquery"], $this->PRODUCT_CODE, $pdamPayment, $PAY["random_number"]);
                    }
                }
            }

        }

        $this->response(array("STATUS" => $this->EVENT_STATUS, "DATA_JSON" => $pdamPayment), REST_Controller::HTTP_OK);
    }

    /** Function To Handle Reversal Request Payment */
    public function PDAMReversalPayment($http_pay, $pCode, $pdamReversal, $random)
    {
        //GetPDAMReversal($http_pay, $pCode, $random)
        $REV = $this->pdamlib->GetPDAMReversal($http_pay, $pCode, $random);

        $this->RESPONSE_MESSAGES = $REV["response_http"];
        $pdamReversal["RESPON_HTTP_REVERSAL"] = $REV["response_http"];
        $pdamReversal["STATUS_TRANSAKSI"] = "REVERSAL";

        if($this->RESPONSE_MESSAGES == "" || $this->RESPONSE_MESSAGES == false)
        {
            $this->EVENT_STATUS = false;
            $pdamReversal["PESAN_KESALAHAN"] = $REV["error_message"];
        }
        else
        {
            $data_iso  = $REV["data_iso"];
            $error_msg = $data_iso["39"];

            if($error_msg == "")
            {
                $this->EVENT_STATUS = false;
                $pdamReversal["PESAN_KESALAHAN"] = "Response Reversal Null. Reversal Failed";
            }

    		if($error_msg == "00")
    			$pdamReversal["PESAN_KESALAHAN"] = "Proses Transaksi Pembayaran Gagal";
            else
            {
                $pdamReversal["PESAN_KESALAHAN"] = $this->GetErrorMessage($error_msg);
                $pdamReversal["DATA_EKSTRAKSI"]  = $this->GetDataArrayPDAM($data_iso["48"], $this->ADMIN_BANK_FEE);
            }
        }

        return $pdamReversal;
    }


}
