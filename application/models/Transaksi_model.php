<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
    protected $temp_transaksi = "temp_transaksi";
    protected $temp_transaksi_detail = "temp_transaksi_detail";
    protected $temp_transaksi_detail_status = "temp_transaksi_detail_status";

    public function insertTransaksi($transaksi, $detail)
    {
        $this->db->insert($this->temp_transaksi, $transaksi);
        $detail = array_push($detail["transaksi_id"], $this->db->insert_id());
        $results = $this->db->insert($this->temp_transaksi_detail, $transaksi);
        if($results)
            return true;
        return false;
    }

    public function getTransaksiByUserID($user_id)
    {
        $query = $this->db->select('*')
                 ->from($this->temp_transaksi . ' T')
                 ->join($this->temp_transaksi . ' TD', 'T.id = TD.transaksi_id');
                 ->where('T.user_id', $user_id)
                 ->get();

        if($query->num_rows() != 0)
            return $query->result_array();

        return false;
    }

    public function getTransaksiByStatus($status)
    {
        $query = $this->db->select('*')
                          ->from($this->temp_transaksi.' T')
                          ->join($this->temp_transaksi_detail.' T', 'T.id = TD.transaksi_id')
                          ->where('T.status_id', $status)
                          ->get();

        if($query->num_rows() != 0)
            return $query->result_array();

        return false;
    }

    public function getTransaksiByDate($date)
    {
        $query = $this->db->select('*')
                          ->from($this->temp_transaksi.' T')
                          ->join($this->temp_transaksi_detail.' TD', 'T.id = TD.transaksi_id')
                          ->where('T.tgl_transaksi', $date)
                          ->get();

        if($query->num_rows() != 0)
            return $query->result_array();

        return false;
    }

    public function getTransaksiByIntervalDate($start_date, $end_date)
    {
        $query = $this->db->select('*')
                          ->from($this->temp_transaksi.' T')
                          ->join($this->temp_transaksi_detail.' TD', 'T.id = TD.transaksi_id')
                          ->where('T.tgl_transaksi >=', $start_date)
                          ->where('T.tgl_transaksi <=', $end_date)
                          ->get();
        if($query->num_rows() != 0)
            return $query->result_array();

        return false;
    }

    public function getTransaksiByUserAndDate($user_id, $date)
    {
        $query = $this->db->select('*')
                 ->from($this->temp_transaksi .' T')
                 ->join($this->temp_transaksi_detail. ' TD', 'T.id = TD.transaksi_id')
                 ->where('T.user_id', $user_id)
                 ->where('T.tgl_transaksi', $date)
                 ->get();

        if($query->num_rows() != 0)
            return $query->result_array();

        return false;
    }

    public function getTransaksiByUserAndIntervalDate($user_id, $start_date, $end_date)
    {
        $query = $this->db->select('*')
                 ->from($this->temp_transaksi. ' T')
                 ->join($this->temp_transaksi_detail. ' TD', 'T.id = TD.transaksi_id')
                 ->where('T.user_id', $user_id)
                 ->where('T.tgl_transaksi >=', $start_date)
                 ->where('T.tgl_transaksi <=', $end_date)
                 ->get();

        if($query->num_rows() != 0)
            return $query->result_array();

        return false;

    }

}
