<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH. "/libraries/excelwriter.inc.php";

class Pdamrekon
{
    protected $DATA_TITLE = array(
        "KODE BANK",
        "MERCHANT TYPE",
        "ID PELANGGAN",
		"AMOUNT",
        "TGL TRX",
        "PERIODE BAYAR",
		"TGL REKON",
		"BILL REPEAT",
		"STAN",
		"ID PDAM",
		"NAMA PDAM"
    );
    protected $DIRECTORY = APPPATH . "/logs/fortuna/";


    protected getFileDirectoryName($filenames)
    {
        return $this->DIRECTORY. $file; // logs/fortuna/INM-REKONPDAM-20161118-TIRTANADI.xls
    }

    protected function getFilenNames($date, $names)
    {
        return "INM-REKONPDAM-".$date."-".$names.".xls"; //INM-REKONPDAM-20161118-TIRTANADI.xls
    }

    public function CreateFileRekon($data_array, $date, $names)
    {
        $filename = $this->getFilenNames($date, $names);
        $file_dir = $this->getFileDirectoryName($filename)
        $excelobj = new ExcelWriter($file_dir);
        $excelobj->writeLine($this->DATA_TITLE);

        for ($i=0; $i <count($data_array); $i++) {
            $excelobj->writeLine($data_array[$i]);
        }
        $excelobj->close();
        return "Data written to file $fileName Successfully.";
    }

}
