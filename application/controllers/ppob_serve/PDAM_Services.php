<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class PDAM_Services extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('fortuna/pdam');
    }

    public function Inquiry_get($nama_produk, $no_pelanggan)
    {
        $status   = false;
        $inquiry  = array();
        $json_data = array();
        $kode_fortuna  = GetPDAMCode($nama_produk);

        $inquiry = $this->pdam->GetInquiryPDAM($no_pelanggan, $kode_fortuna);

        $inquiry["kode_fortuna"]  = $kode_fortuna;
        $inquiry["nama_pdam"]     = GetPDAMNames($nama_produk);
        $inquiry["nama_propinsi"] = GetPDAMProvince($nama_produk);
        $admin_fee = GetPDAMAdminFee($nama_produk);

        if($inquiry["respon_status"] == false)
            $inquiry["pesan_error"] = "Proses Time Out. Koneksi Server Terputus";
        else
        {
            $inquiry["kode_error"] = $inquiry["data_iso"]["39"];

            if(!$this->session->userdata($nama_produk."_".$no_pelanggan))
                $this->session->set_userdata($nama_produk."_".$no_pelanggan, $inquiry["respon_http"]);

            if($inquiry["kode_error"] == "00")
            {
                $status = true;
                $inquiry["pesan_error"]   = $this->pdam->GetErrorMessage($inquiry["kode_error"]);
                $inquiry["ekstraksi_bit"] = $this->pdam->UpgradeBitToDataArray($inquiry["data_iso"]["48"], $admin_fee);

                $json_data = $this->pdam->SetAbsoluteDataArray($inquiry);
            }
            else {
                $inquiry["pesan_error"] = $this->pdam->GetErrorMessage($inquiry["kode_error"]);
            }

        }

        $this->response(array("status"=>$status, "json_inquiry" => $inquiry, "session_pdam" => $this->session->userdata($nama_produk."_".$no_pelanggan), "data_table" => $json_data), REST_Controller::HTTP_OK);
    }

    public function Payment_get($nama_produk, $no_pelanggan)
    {
        $respon_http = "";
        $status   = false;
        $payment  = array();
        $kode_fortuna  = GetPDAMCode($nama_produk);

        if(!$this->session->userdata($nama_produk."_".$no_pelanggan))
        {
            $payment['pesan_error'] = 'Proses Payment Gagal. No Pelanggan '. $no_pelanggan . ' tidak masuk dalam cart';
        }
        else
        {
            $respon_http = $this->session->userdata($nama_produk."_".$no_pelanggan);
            $payment = $this->pdam->GetPaymentPDAM($no_pelanggan, $kode_fortuna, $respon_http);

            if($payment["respon_status"] == true)
            {
                $data_pay = $payment["data_iso_jak"];

                if($data_pay["39"] == "00")
                {
                    $status = true;
                    $payment["kode_error"]   = $data_pay["39"];
                    $payment["pesan_error"]  = $this->pdam->GetErrorMessage($data_pay["39"]);
                    $payment["status_error"] = "PAYMENT";
                }
                else
                {
                    if($data_pay["39"]=="86")
                    {
                        $payment["kode_error"]   = $data_pay["39"];
                        $payment["pesan_error"]  = $this->pdam->GetErrorMessage($data_pay["39"]);
                        $payment = $this->pdam->GetPDAMReversal($payment);
                    }
                }
            }
        }

        $this->response(array("status"=>$status, "json_payment" => $payment), REST_Controller::HTTP_OK);


    }

}
