<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.css">
    <link rel="stylesheet" href="<?php echo site_url('assets/templates/css/styles.css'); ?>">

    <script type="text/javascript">
        window.base_url = "<?php echo base_url(); ?>";
        window.cart = null;
    </script>

</head>

<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#"><?php echo $subtitle; ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url('pdam/beranda') ?>">PDAM</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo site_url('bpjs/beranda') ?>">BPJS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">PLN</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">PULSA</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <h2 class="my-4"><?php echo $headings; ?></h2>
    <div class="row">
        <div class="col-sm-12">
            <div id="headContent">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="nama_produk">Produk &nbsp;</label>
                        <select id="nama_produk" name="nama_produk" class="form-control">
                        <?php foreach ($inisial_produk as $key => $value) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    &nbsp;
                    <div class="form-group">
                        <label for="no_pelanggan">No Pelanggan &nbsp;</label>
                        <input type="text" class="form-control
                        " id="no_pelanggan" name="no_pelanggan" placeholder="No Pelanggan">
                    </div>
                    &nbsp;

                    &nbsp;
                    <div class="form-group">
                        <label for="jumlah_bulan">Jumlah Bulan &nbsp;</label>
                        <select id="jumlah_bulan" name="jumlah_bulan" class="form-control">
                        <?php for ($i=0; $i < 12; $i++) { ?>
                            <option value="<?php echo $i+1; ?>" class="<?php if($i==0) echo 'active' ?>"><?php echo $i+1 . " Bulan"; ?></option>
                        <?php } ?>
                        </select>
                    </div>

                    &nbsp;
                    <div class="form-group">
                        <button type="submit" id="btn_check" class="btn btn-primary">Check</button> &nbsp;
                        <button type="submit" id="btn_bayar" class="btn btn-success">Bayar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="contents">
                <div id="canvas"></div>
            </div>
        </div>
    </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.0/jquery-confirm.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.17.1/axios.min.js"></script>
<script src="<?php echo site_url('assets/templates/js/app.js'); ?>"></script>
<script src="<?php echo site_url('assets/templates/js/bpjs.js'); ?>"></script>

</body>
</html>
