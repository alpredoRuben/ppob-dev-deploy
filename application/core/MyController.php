<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyController extends CI_Controller
{
    protected $user_auth = 'admin';
    protected $user_password = '1234';
    protected $baseUrl = 'http://pay-inm.co.id/inm_lab/ppob/';

    protected $OAUTH = array(
        "grant_type" => "authorization_code",
        "client_id"  => null,
        "client_secret" => null,
        "code" => null
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function getRestHTTPCurl($str)
    {
        $site_url    = $this->baseUrl . $str;
        $curl_handle = curl_init();

        curl_setopt($curl_handle, CURLOPT_URL, $site_url);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl_handle, CURLOPT_USERPWD, "$this->user_auth:$this->user_password");
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 10);
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);

        $response = json_decode($response, true);
        return $response;
    }

    public function RestHTTPCurlPOSTWithOAuth($url, $client_id, $client_secret, $code)
    {
        $response = null;

        $this->OAUTH["client_id"] = $client_id;
        $this->OAUTH["client_secret"] = $client_secret;
        $this->OAUTH['code'] = $code

        $curl_handle = curl_init($url);
        curl_setopt($curl_handle, CURLOPT_POST, TRUE);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $this->OAUTH);
        curl_setopt($curl_handle, CURLOPT_TIMEOUT, 10);
        $response = curl_exec($curl_handle);
        curl_close($curl_handle);

        $response = json_decode($response, true);
        return $response;
    }

    public function setOutputJson($data)
    {
        $this->output->set_content_type('application/json')->set_output(json_encode( $data ));
        return;
    }

}
