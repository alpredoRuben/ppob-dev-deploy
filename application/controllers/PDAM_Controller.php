<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PDAM_Controller extends FortunaController
{
    protected $HTTP_REST_API ="http://pay-inm.co.id/inm_lab/ppob/";

    public function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
    }

    public function index()
    {
        $this->session->sess_destroy();

        $data = array(
            'title' => 'INM PPOB',
            'subtitle' => 'PPOB DEVELOP',
            'headings' => 'PDAM CONTROLLER',
            'inisial_produk' => GetPDAMNames()
        );

        $this->load->view('ppob/pdam_view', $data);
    }

    public function pdam_inquiry()
    {
        $post = json_decode(file_get_contents('php://input'), true);

        $no_pelanggan = $post["no_pelanggan"];
        $nama_produk  = $post["nama_produk"];

        $str_url  = $this->getPdamInquery($nama_produk, $no_pelanggan);
        redirect($str_url);
        // $response = $this->getRestHTTPCurl($str_url);
        //
        // $this->setOutpuJson($response);
    }

    public function pdam_payment()
    {
        $post = json_decode(file_get_contents('php://input'), true);

        $no_pelanggan = $post["no_pelanggan"];
        $nama_produk  = $post["nama_produk"];

        $str_url  = $this->getPdamPayment($nama_produk, $no_pelanggan);
        redirect($str_url);
        // $response = $this->getRestHTTPCurl($str_url);
        //
        // $this->setOutpuJson($response);
    }

    public function getPdamInquery($product_name, $customer_id)
    {
        return "rest/get/pdam/inquiry/". $product_name ."/". $customer_id;
    }

    public function getPdamPayment($product_name, $customer_id)
    {
        return "rest/get/pdam/payment/". $product_name ."/". $customer_id;
    }


}
