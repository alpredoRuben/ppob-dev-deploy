<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends FortunaController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("product_model");
    }

    public function setRecordDatabaseBPJSPoduct()
    {
        $product   = GetBPJSCode();
        $temporary = array();
        $index = 301;
        foreach ($product as $key => $value)
        {
            $temporary["jenis_produk_id"]    = 3;
            $temporary["nama_singkat"]       = $key;
            $temporary["nama_lengkap"]       = GetBPJSNames($key);
            $temporary["kode_produk"]        = $index;
            $temporary["vendor"]             = "Fortuna";
            $temporary["kode_vendor"]        = 2;
            $temporary["kode_produk_vendor"] = $value;
            $temporary["keterangan"]         = "Biaya Admin Bank = " . GetBPJSAdminFee($key);
            $this->product_model->insertProduct($temporary);
            $index ++;
        }
    }

    public function getRecordDatabaseProduct()
    {
        $data = $this->product_model->getRecordProductAll();
        if($data != null)
            $this->setOutputJson($data);
    }

    public function setRecordDatabasePDAMPoduct()
    {
        $product   = GetPDAMCode();
        $temporary = array();
        $index = 201;
        foreach ($product as $key => $value)
        {
            $temporary["jenis_produk_id"]    = 2;
            $temporary["nama_singkat"]       = $key;
            $temporary["nama_lengkap"]       = GetPDAMNames($key);
            $temporary["kode_produk"]        = $index;
            $temporary["vendor"]             = "Fortuna";
            $temporary["kode_vendor"]        = 2;
            $temporary["kode_produk_vendor"] = $value;
            $temporary["keterangan"]         = "Biaya Admin Bank = " . GetPDAMAdminFee($key);
            $this->product_model->insertProduct($temporary);
            $index ++;
        }
    }

}
