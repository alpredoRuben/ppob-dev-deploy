<?php defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH. "/libraries/JAK8583/JAK8583.php";

class Bpjslib
{
    /** IP:Port [Production = "8080"] and [Development = "4101"] */
    protected $IP_PORT = "http://localhost:4101?q=";

    /** Constanta Result Response Code 48 Bit Length */
    protected $CONSTANTA_LENGTH  = 134;

    /** Bit Code 7 - Transmision Date Time */
    protected $TRANSMISSION_DATETIME = null;

    /** Bit Code 7 - Transmision Date Time */
    protected $REFERENCE_NUMBER = null;

    /** Bit Code 18 - Merchant Code */
    protected $MERCHANT_CODE = array(
        "teller"      => "6010",
        "atm"         => "6011",
        "edc"         => "6012",
        "auto_debit"  => "6013",
        "e_banking"   => "6014",
        "kiosk"       => "6015",
        "phone_bank"  => "6016",
        "mobile_bank" => "6017"
    );

    /** Bit Code 32 - Acquiring Institution ID Code */
    protected $INSTITUTION_CODE = "20015";

    /** Bit Code 3 - Processing Code */
    protected $PROCESSING_CODE = array(
        "bill_inquiry"   => "380000",
        "payment_cash"   => "501000",
        "commit_payment" => "502000"
    );

    /** Bit Code 41 - Card Acceptor Terminal ID */
    protected $CARD_ID = "101INM02";

    /** Function To Add Zero Left */
    public function ToAddZero($text)
    {
        $temporary = '';
    	for ($i=1; $i<5-strlen($text); $i++){
    		$temporary = $temporary. '0';
    	}
    	return($temporary . $text);
    }

    /** Function To Add Zero Bit Length With Character 0 */
    function AddZeroBit($text, $length)
    {
    	$temporary = '';
    	for ($i=0; $i < $length - strlen($text); $i++){
    		$temporary = $temporary.'0';
    	}
    	return($temporary . $text);
    }


    /** Function To Get Response HTTP Curl */
    public function GetHTTPCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    /** Function To Unset Data ISO */
    public function ToUnsetDataISO($data, $index)
    {
        for ($i=0; $i < count($index); $i++) {
            unset($data[$index[$i]]);
        }
        return $data;
    }

    /** Function To Convert Numeric Code Error To String Message */
    public function GetErrorMessage($code)
    {
        $data = array(
            '00' => 'Transaksi Disetujui',
            '06' => 'Reversal Ditolak. Transaksi Asli Tidak Ditemukan',
            '10' => 'Pelanggan Tidak Ditemukan',
            '13' => 'Transaksi Tidak Dapat Dilakukan',
            '14' => 'Transaksi Harus Dilakukan Di Kantor PDAM',
            '30' => 'Database Error',
            '50' => 'Jumlah Tidak Valid',
            '86' => 'Timeout',
            '88' => 'Tagihan Sudah Terbayar',
            '89' => 'Error Link',
            '94' => 'Reversal Ditolak, Transaksi Sudah Dibatalkan',
            '99' => 'Error Lain Lain'
        );
        return $data[$code];
    }

    /** Extraction Bit 48 Message Response To Array  */
    public function ExtractBitBPJSKesehatan($bit_msg)
    {
        $ex  = array();
        $tmp = array();

        $ex = array(
            "billing_id"    => trim(substr($bit_msg, 0, 16)),
            "month_bill"    => intval(substr($bit_msg, 16, 2)),
            "nokapst"       => substr($bit_msg, 18, 13),
            "bill_repeat"   => intval(substr($bit_msg, 31, 2)),
            "repeat_data"   => substr($bit_msg, 33, strlen($bit_msg)),
            "commit_number" => null,
            "data_info"     => array()
        );

        $repeat_length  = strlen($ex["repeat_data"]);
        $default_length = $ex["bill_repeat"] * $this->CONSTANTA_LENGTH;
        $diff_length    = $repeat_length - $default_length;

        $data_loop = "";

        if($repeat_length % $this->CONSTANTA_LENGTH == 0){
            $data_loop = $ex["repeat_data"];
            $ex["number_refference"] = "";
        }
        else{
            $data_loop = substr( $ex["repeat_data"], 0, ($repeat_length - $diff_length) );
            $ex["number_refference"] = substr($ex["repeat_data"], $default_length, $diff_length);
        }

        $index = 0;
        for ($i=0; $i < $ex["bill_repeat"]; $i++){
            $tmp[$i] = substr($data_loop, $index, $this->CONSTANTA_LENGTH);
            $index += $this->CONSTANTA_LENGTH;
        }

    	for ($i = 0; $i < $ex["bill_repeat"]; $i++)
        {
            $bpjs = array(
                "CUSTOMER_ID"     => substr($tmp[$i],   0, 16),
                "CUSTOMER_NAME"   => trim(substr($tmp[$i],  16, 35)),
                "PREMI_ID"        => trim(substr($tmp[$i],  51,  8)),
                "BRANCH_ID"       => trim(substr($tmp[$i],  59,  4)),
                "BRANCH_N"        => trim(substr($tmp[$i],  63, 35)),
                "PREMI_DIMUKA"    => intval(substr($tmp[$i], -36, 12)),
                "PREMI_BULAN_INI" => intval(substr($tmp[$i], -24, 12)),
                "BILL_AMOUNT"     => intval(substr($tmp[$i], -12))
            );

            array_push($ex["data_info"], $bpjs);
    	}

        return $ex;
    }

    /** Get and Control Array Data Request Inquiry */
    public function GetArrayBPJSKesehatan($bit, $admin_fee)
    {
        $results = array();
        $bitlength = strlen($bit);
        if($bitlength > 0)
        {
            $extract = $this->ExtractBitBPJSKesehatan($bit);
            $results["NO_REFERENSI"]            = $extract["number_refference"];
            $results["NO_BILL_PELANGGAN"]       = $extract["billing_id"];
            $results["NO_AKUN_VIRTUAL"]         = $extract["data_info"][0]["CUSTOMER_ID"];
            $results["BULAN_TAGIHAN"]           = $extract["month_bill"];
            $results["NOKAPST"]                 = $extract["nokapst"];
            $results["TOTAL_PESERTA_BPJS"]      = $extract["bill_repeat"];
            $results["REPEAT_DATA"]             = $extract["repeat_data"];
            $results["WAKTU_TRANSAKSI"]         = date("d F Y");
            $results["BIAYA_ADMIN"]             = $admin_fee;
            $results["NAMA_PENDAFTAR_BPJS"]     = $extract["data_info"][0]["CUSTOMER_NAME"];
            $results["STRUK_INFORMASI"] = array();

            $total = 0;
            foreach ($extract["data_info"] as $key => $value)
            {
                $tmp = array();
                $total_tagihan = 0;
                foreach ($value as $k => $v)
                {
                    $tmp["ID_PELANGGAN"]          = $value["CUSTOMER_ID"];
                    $tmp["NAMA_PELANGGAN"]        = $value["CUSTOMER_NAME"];
                    $tmp["ID_PREMI"]              = $value["PREMI_ID"];
                    $tmp["ID_BRANCH"]             = $value["BRANCH_ID"];
                    $tmp["NAMA_BRANCH"]           = $value["BRANCH_N"];
                    $tmp["BIAYA_PREMI_DIMUKA"]    = $value["PREMI_DIMUKA"];
                    $tmp["BIAYA_PREMI_BULAN_INI"] = $value["PREMI_BULAN_INI"];
                    $tmp["BIAYA_TAGIHAN"]         = $value["BILL_AMOUNT"];

                    $total_tagihan = $tmp["BIAYA_TAGIHAN"];
                }

                $total += $total_tagihan;
                array_push($results["STRUK_INFORMASI"], $tmp);
            }

            $results["JUMLAH_BIAYA_TAGIHAN"]  = $total;
            $results["TOTAL_SELURUH_TAGIHAN"] = $total + $admin_fee;

            return $results;
        }
        else
            return null;

    }

    private function ConvertResponToISO($response)
    {
        $JAK = new JAK8583();
        $JAK->addISO(substr($response, 4, strlen($response)));
        $iso = $JAK->getData();
        return $iso;
    }

    /** Function To Get Request Inquiry BPJS Kesehatan **/
    public function GetBPJSInquiry($customer_id, $pCode, $month)
    {
        date_default_timezone_set('Asia/Jakarta');

        if(intval($month) < 10)
            $month = '0'.$month;

        $JAK = new JAK8583();
        $INQ = array();

        $random  = mt_rand(1000, 999999);

        $JAK->addMTI('0200');
        $JAK->addData(3,  $this->PROCESSING_CODE["bill_inquiry"]);
        $JAK->addData(4,  '000000000000');
        $JAK->addData(7,  gmdate("mdHis"));
        $JAK->addData(11, $random);
        $JAK->addData(12, date("His"));
        $JAK->addData(13, date("md"));
        $JAK->addData(15, date('md', strtotime(date('md')) + 86400));
        $JAK->addData(18, $this->MERCHANT_CODE["edc"]);
        $JAK->addData(32, $this->INSTITUTION_CODE);
        $JAK->addData(37, date("His").$random);
        $JAK->addData(41, $this->CARD_ID);
        $JAK->addData(48, $customer_id . $month);
        $JAK->addData(49, '360');
        $JAK->addData(63, $pCode);

        $INQ["messages_iso"] = $this->ToAddZero(strlen($JAK->getISO())) . $JAK->getISO();
        $INQ["response_http"] = $this->GetHTTPCurl($this->IP_PORT . urlencode($INQ["messages_iso"])."&stan=".$random."&mType=".'0200');

        if($INQ["response_http"] == "" || $INQ["response_http"] == false)
        {
            $INQ["error_message"] = "Proses Time Out. Koneksi Server Terputus";
        }
        else
        {
            $JAK->addISO(substr($INQ["response_http"], 4, strlen($INQ["response_http"])));
            $INQ["data_iso"] = $JAK->getData();
        }

        return $INQ;
    }

    /** Function To Get Process Request Payment BPJS*/
    public function GetBPJSPayment($pCode, $http)
    {
        date_default_timezone_set('Asia/Jakarta');

        $number  = array("3","7","11","12","15","37","39","61","63");

        $PAY = array();
        $JAK = new JAK8583();

        $dataInq = $this->ConvertResponToISO($http);
        $dataInq = $this->ToUnsetDataISO($dataInq, $number);

        $random = mt_rand(1000, 999999);

        $JAK->addMTI('0200');
    	$JAK->addData(3,  $this->PROCESSING_CODE["payment_cash"]);
        $JAK->addData(7,  gmdate("mdHis"));
    	$JAK->addData(11, $random);
    	$JAK->addData(12, date("His"));
    	$JAK->addData(15, date('md', strtotime(date('md')) + 86400));
    	$JAK->addData(37, date("His").$random);

    	foreach ($dataInq as $key => $val) {
    		$JAK->addData($key, $val);
    	}

        $JAK->addData(63, $pCode);

        $PAY["messages_iso"]  = $this->ToAddZero(strlen($JAK->getISO())) . $JAK->getISO();
        $PAY["response_http"] = $this->GetHTTPCurl($this->IP_PORT.urlencode($PAY["messages_iso"])."&stan=".$random."&mType=".'0200');

        if($PAY["response_http"] == "" || $PAY["response_http"] == false)
        {
            $PAY["error_message"] = "Transaksi Pembayaran BPJS Gagal. Koneksi Server Terputus";
        }
        else
        {
            $JAK->addISO(substr($PAY["response_http"], 4, strlen($PAY["response_http"])));

	        $PAY["data_iso"] = $JAK->getData();
            $PAY["random_number"] = $random;
        }

        return $PAY;
    }

    /** Function To Get Process Request Reversal Payment BPJS Kesehatan */
    public function GetBPJSReversal($pCode, $http)
    {
        $number   = array("3", "7", "11", "12", "15", "37", "39", "61", "63");

        $REV = array();

        $JAK = new JAK8583();

        $dataRev = $this->ConvertResponToISO($http);
        $dataRev = $this->ToUnsetDataISO($dataRev, $number);

        $random = mt_rand(1000, 999999);

        $JAK->addMTI('0200');
    	$JAK->addData(3,  $this->PROCESSING_CODE["payment_cash"]);
    	$JAK->addData(7,  gmdate("mdHis"));
    	$JAK->addData(11, $random);
    	$JAK->addData(12, date("His"));
    	$JAK->addData(15, date('md', strtotime(date('md')) + 86400));
        $JAK->addData(37, date("His").$random);

    	foreach ($dataRev as $key => $val){
    		$JAK->addData($key, $val);
        }

    	$JAK->addData(63, $pCode);

        $REV["messages_iso"] = $this->ToAddZero(strlen($JAK->getISO())).$JAK->getISO();
        $REV["response_http"] = $this->GetHTTPCurl($this->IP_PORT.urlencode($REV["messages_iso"])."&stan=".$random."&mType=".'0200');

        if($REV["response_http"] =='' || $REV["response_http"] == false)
        {
            $REV["error_message"] = "Transaksi Pembayaran Gagal";
        }
        else
        {
            $JAK->addISO(substr($REV["response_http"], 4, strlen($REV["response_http"])));
	        $REV["data_iso"] = $JAK->getData();
        }

        return $REV;
    }

    /** Function To Get Process Authentication Commit Event Process Payment Success **/
    public function GetBPJSCommitPayment($http, $product_code)
    {
        $number   = array("3", "7", "11", "12", "15", "37", "39","63");

        $COMM = array();
        $JAK = new JAK8583();

        $dataPay = $this->ConvertResponToISO($http);
        $dataPay = $this->ToUnsetDataISO($dataPay, $number);

        $random = mt_rand(1000, 999999);
        $bit7  = '';
        $bit32 = '';

        $JAK->addMTI('0200');
        $JAK->addData(3, $this->PROCESSING_CODE["commit_payment"]);
        $JAK->addData(7, gmdate("mdHis"));
        $JAK->addData(11, $random);
        $JAK->addData(12, date("His"));
        $JAK->addData(15, date('md', strtotime(date('md')) + 86400));
        $JAK->addData(37, date("His").$random);

        foreach ($dataPay as $key => $val)
        {
            if($key==7)
		        $this->TRANSMISSION_DATETIME = $val;

            if($key==32)
		        $this->REFERENCE_NUMBER = $this->AddZeroBit($val,11);

            $COMM->addData($key, $val);
        }

        $JAK->addData(63, $product_code);
        $bit90 = "0200". $random. $this->TRANSMISSION_DATETIME. $this->REFERENCE_NUMBER. "00000000000";
        $JAK->addData(90, $bit90);

        $COMM["messages_iso"] = $this->ToAddZero(strlen($JAK->getISO())).$JAK->getISO();
        $COMM["response_http"] = $this->GetHTTPCurl($this->IP_PORT.urlencode($COMM["messages_iso"])."&stan=".$random."&mType=".'0200');

        if($COMM["response_http"] == "" || $COMM["response_http"] == false)
        {
            $COMM["error_message"] = "Transaksi Pembayaran Gagal";
        }
        else
        {
            $JAK->addISO(substr($COMM["response_http"], 4, strlen($COMM["response_http"])));
	        $COMM["data_iso"] = $JAK->getData();
            $COMM["random_number"] = $random;
        }

        return $COMM;
    }






















}
