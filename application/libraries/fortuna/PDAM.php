<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pdam
{

    protected $ip_port   = "http://localhost:4000?q=";

    protected $MERCHANT_CODE = array(
        "teller"      => "6010",
        "atm"         => "6011",
        "edc"         => "6012",
        "auto_debit"  => "6013",
        "e_banking"   => "6014",
        "kiosk"       => "6015",
        "phone_bank"  => "6016",
        "mobile_bank" => "6017"
    );

    protected $INSTITUTION_CODE = "20015";

    protected $PROCESSING_CODE = array(
        "bill_inquiry"   => "380000",
        "payment_cash"   => "501000",
        "payment_cheque" => "502000",
        "payment_credit" => "503000"
    );

    protected $CARD_ID = "101INM02";

    /** Function To Add Zero Bit */
    public function ToAddZeroBit($text, $ln)
    {
        $rs='';
        for ($i=0; $i<$ln-strlen($text); $i++)
        {
        	$rs=$rs.'0';
        }
        return($rs.$text);
    }

    /** Function To Unset Data ISO */
    public function ToUnsetDataISO($data, $index)
    {
        for ($i=0; $i < count($index); $i++) {
            unset($data[$index[$i]]);
        }
        return $data;
    }

    /** Function To Conver Numeric Code To String Message */
    public function GetErrorMessage($code_error)
    {
        $data = array(
            '00' => 'Transaksi Disetujui',
            '06' => 'Reversal Ditolak',
            '10' => 'Pelanggan Tidak Ditemukan',
            '13' => 'Transaksi Tidak Dapat Dilakukan',
            '14' => 'Transaksi Harus Dilakukan Di Kantor PDAM',
            '22' => 'Permintaan Saran Gagal',
            '50' => 'Jumlah Tidak Valid',
            '30' => 'Database Error',
            '86' => 'Timeout',
            '88' => 'Tagihan Sudah Terbayar',
            '89' => 'Error Link',
            '91' => 'Link Down',
            '92' => 'Potongan Tagihan (Cut off Biller)',
            '94' => 'Reversal Ditolak',
            '97' => 'Transaksi Gagal, Tagihan Sudah Dibayar',
            '98' => 'Transaksi Gagal, Respon Tagihan Tidak Terdefenisi',
            '99' => 'Error Lain Lain'
        );
        return $data[$code_error];
    }

    /** Function To Add Zero Left */
    public function ToAddZero($str)
    {
        $tmp = '';
        for ($i=1; $i<5-strlen($str); $i++)
        {
            $tmp .= '0';
        }
        return($tmp . $str);
    }

    /** Function To Get Response HTTP Curl */
    public function GetHTTPCurl($url)
    {
        $curls = curl_init();
        curl_setopt($curls, CURLOPT_URL, $url);
        curl_setopt($curls, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curls, CURLOPT_TIMEOUT, 60);
        $response_messages = curl_exec($curls);
        curl_close($curls);
        return $response_messages;
    }

    /** Extraction Bit 48 Message Response To Array data_info Customer */
    public function ToExtractionBit48PDAM($bit_msg)
    {
        $ekstrak_bit = array();

        if(strlen($bit_msg) > 0)
        {
            $ekstrak_bit["no_pelanggan"]   = trim(substr($bit_msg,0,15)); //billing_id
            $ekstrak_bit["nama_pelanggan"] = str_replace("  ","",substr($bit_msg, 15, 30));
            $ekstrak_bit["alamat"]         = str_replace("  ","",substr($bit_msg, 45, 30));
            $ekstrak_bit["no_tarif"]      = substr($bit_msg, 75, 5);
            $ekstrak_bit["status_tagihan"] = intval(substr($bit_msg, 80, 2));
            $ekstrak_bit["repeat_data"]	   = substr($bit_msg, 82, strlen($bit_msg));
            $ekstrak_bit["data_info"]      = array();

            $start = 0 ;
            for ($i = 0; $i < intval($ekstrak_bit["status_tagihan"]); $i++)
            {
               $tmp = array(
                   'info_tagihan'    => substr($ekstrak_bit["repeat_data"], $start, 1),
                   'periode_tagihan' => substr($ekstrak_bit["repeat_data"], $start + 1, 6),
                   'akun_tagihan'    => substr($ekstrak_bit["repeat_data"], $start + 7, 10),
                   'biaya_tagihan'   => intval(substr($ekstrak_bit["repeat_data"], $start + 17, 12)),
                   'biaya_denda'     => intval(substr($ekstrak_bit["repeat_data"], $start + 29, 8)),
                   'stan_meter'	     => intval(substr($ekstrak_bit["repeat_data"], $start + 37, 8)),
                   'besar_pemakaian' => intval(substr($ekstrak_bit["repeat_data"], $start + 46, 8))
               );

               $start = $start + 54;
               array_push($ekstrak_bit["data_info"], $tmp);
            }
        }

        return $ekstrak_bit;
    }

    /** Get and Control Data Request Inquiry */
    public function UpgradeBitToDataArray($bit_msg, $admin_fee)
    {
        $data = $this->ToExtractionBit48PDAM($bit_msg);
        $data["waktu_transaksi"] = date("Y-m-d H:i:s");
        $data["total_ekstraksi"] = count($data["data_info"]);
        $data["biaya_admin"]     = $admin_fee;
        $data["struk_info"]      = array();

        $total = 0;
        foreach ($data["data_info"] as $key => $value)
        {
            $tmp = array();
            $total_tagihan = 0;

            foreach ($value as $k => $v)
            {
                $tmp["info_tagihan"]     = $value["info_tagihan"];
                $tmp["periode_tagihan"]  = ConvertToIndonesiaFormatDate(SetMonthYear($value["periode_tagihan"]));
                $tmp["biaya_tagihan"]    = intval($value["biaya_tagihan"]);
                $tmp["biaya_denda"]      = intval($value["biaya_denda"]);
                $tmp["stan_meter_awal"]  = (intval($value["stan_meter"]) - intval($value["besar_pemakaian"]));
                $tmp["stan_meter_akhir"] = intval($value["stan_meter"]);
                $tmp["besar_pemakaian"]  = intval($value["besar_pemakaian"]);
                $tmp["total_tagihan"]    = (intval($value["biaya_tagihan"]) + intval($value["biaya_denda"]) + $admin_fee);
                $total_tagihan = $tmp["total_tagihan"];
            }

            $total += $total_tagihan;
            array_push($data["struk_info"], $tmp);
        }

        $data["total_biaya_admin"]   = $admin_fee * intval($data["status_tagihan"]);
        $data["total_biaya_tagihan"] = $total;
        return $data;

    }

    /** Function To Convertion Array Data To Data Table*/
    public function SetAbsoluteDataArray($arrays)
    {
        $absolutes = array();
        $eks = $arrays["ekstraksi_bit"];

        $absolutes["respon_http"]    = $arrays["respon_http"];
        $absolutes["pesan_iso"]      = $arrays["pesan_iso"];
        $absolutes["kode_fortuna"]   = $arrays["kode_fortuna"];
        $absolutes["nama_pdam"]      = $arrays["nama_pdam"];
        $absolutes["nama_propinsi"]  = $arrays["nama_propinsi"];
        $absolutes["pesan_error"]    = $arrays["pesan_error"];
        $absolutes["kode_error"]     = $arrays["kode_error"];

        $absolutes["no_pelanggan"]   = $eks["no_pelanggan"];
        $absolutes["nama_pelanggan"] = $eks["nama_pelanggan"];
        $absolutes["no_tarif"]       = $eks["no_tarif"];
        $absolutes["lembar"]         = $eks["status_tagihan"];
        $absolutes["biaya_admin"]    = $eks["biaya_admin"];
        $absolutes["total_tagihan"]  = $eks["total_biaya_tagihan"];

        $periode        = "";
        $stan_awal      = 0;
        $stan_akhir     = 0;
        $biaya_tagihan  = 0;
        $biaya_denda    = 0;
        $pemakaian      = 0;

        $tmp = $eks["struk_info"];

        for ($i=0; $i <= count($tmp)-1; $i++)
        {
            foreach ($tmp[$i] as $key => $value)
            {
                if($i == 0)
                {
                    $periode    = $tmp[$i]["periode_tagihan"];
                    $stan_awal  = $tmp[$i]["stan_meter_awal"];
                    $stan_akhir = $tmp[$i]["stan_meter_akhir"];
                }
                else
                {
                    if($i == $absolutes["lembar"]-1)
                    {
                       $periode   .= " - ".$tmp[$i]["periode_tagihan"];
                       $stan_akhir = $tmp[$i]["stan_meter_akhir"];
                    }
                }

            }
            $biaya_tagihan += intval($tmp[$i]["biaya_tagihan"]);
            $biaya_denda   += intval($tmp[$i]["biaya_denda"]);
            $pemakaian     = $tmp[$i]["besar_pemakaian"];

        }

        $absolutes["periode_tagihan"]  = $periode;
        $absolutes["stan_meter_awal"]  = $stan_awal;
        $absolutes["stan_meter_akhir"] = $stan_akhir;
        $absolutes["biaya_tagihan"]    = $biaya_tagihan;
        $absolutes["biaya_denda"]      = $biaya_denda;
        $absolutes["besar_pemakaian"]  = $pemakaian;

        return $absolutes;
    }

    /** Function To Run Request Reversal Payment */
    public function GetPDAMReversal($revers, $dateTimes=null)
    {
        $number   = array("3", "12", "13", "15", "37", "39", "61", "63");

        require APPPATH. "/libraries/JAK8583/JAK8583.php";

        $jak = new JAK8583();
        $pay = new JAK8583();

        $revers['keterangan'] = 'reversal';

        $jak->addISO(substr($revers["respon_http_inquiry"], 4, strlen($revers["respon_http_inquiry"])));
        $data = $jak->getData();
        $data = $this->ToUnsetDataISO($data, $number);

        $pay->addMTI("0400");
        $pay->addData(3, $this->PROCESSING_CODE["payment_cash"]);
        $pay->addData(7, gmdate("mdHis"));
        $pay->addData(11, $revers["random_stan"]);
        $pay->addData(12, date("His"));
        $pay->addData(13, date("md"));
        $pay->addData(15, date("md", strtotime(date("md")) + 86400));
        $pay->addData(37, date("His").$revers["random_stan"]);

        $bit_msg_90 = "";
        $bit_msg_4  = 0 ;
        $bit_msg_32 = "";
        $bit_msg_11 = "";

        foreach ($data as $key => $val)
        {
            if($key == 4)
                $bit_msg_4 = intval(substr($val, 0 , 10));

            if($key == 7)
                $dateTimes = $val;

            if($key == 32)
                $bit_msg_32 = $this->ToAddZeroBit($val, 11);

            if($key == 11)
                $bit_msg_11 = $val;

            $pay->addData($key, $val);

        }

        $pay->addData(63, $revers["kode_fortuna"]);
        $bit_msg_90 = "0200" . $random . $dateTimes . $bit_msg_32 . $this->ToAddZeroBit("", 11);
        $pay->addData(90, $bit_msg_90);

        $revers["pesan_iso_reversal"] = addzero(strlen($pay->getISO())) . $pay->getISO();
        $revers["respon_http_reversal"] = httpGet($this->ip_port.urlencode($revers["pesan_iso_reversal"])."&stan=".$random."&mType="."0400");


        if($revers["respon_http_reversal"] == "")
        {
            $revers["pesan_error_reversal"] = "Reversal Time Out. Server Tidak Terkoneksi. Silahkan Hubungi PT. INM";
        }
        else
        {
            $jak->addISO(substr($revers["respon_http_reversal"],4,strlen($revers["respon_http_reversal"])));
            $data_reversal = $jak->getData();

            $revers["kode_error_reversal"] = $data_reversal["39"];

            if($revers["kode_error_reversal"] == "")
                $revers["pesan_error_reversal"] = "Response Reversal Null. Reversal Failed";

    		if($revers["kode_error_reversal"] == "00")
    			$revers["pesan_error_reversal"] = "Proses Transaksi Gagal";
    		else
    			$revers["pesan_error_reversal"] = $this->GetErrorMessage($reversal["kode_error_reversal"]);
        }

        return $reversal;
    }

    /** Function To Prosess Request Inquiry PDAM */
    public function GetInquiryPDAM($no_pelanggan, $pdam_code)
    {
        require APPPATH. "/libraries/JAK8583/JAK8583.php";
        $jak   = new JAK8583();
        $inquiry = array();

        date_default_timezone_set("Asia/Jakarta");

        $jak->addMTI("0200");
        $jak->addData(3, $this->PROCESSING_CODE["bill_inquiry"]);
        $jak->addData(4, "000000000000");
        $jak->addData(7, gmdate("mdHis"));
        $randoms = mt_rand(1000, 999999);
        $jak->addData(11, $randoms);
        $jak->addData(12, date("His"));
        $jak->addData(13, date("md"));
        $jak->addData(15, date("md", strtotime(date("md")) + 86400));
        $jak->addData(18, $this->MERCHANT_CODE["edc"]);
        $jak->addData(32, $this->INSTITUTION_CODE);
        $jak->addData(37, date("His").$randoms);
        $jak->addData(41, $this->CARD_ID);
        $jak->addData(48, $no_pelanggan);
        $jak->addData(49, "360");
        $jak->addData(63, $pdam_code);

        $inquiry["pesan_iso"] = $this->ToAddZero(strlen($jak->getISO())).$jak->getISO();
        $inquiry["respon_http"] = $this->GetHTTPCurl($this->ip_port . urlencode($inquiry["pesan_iso"])."&stan=".$randoms."&mType="."0200");

        if($inquiry["respon_http"] == "")
            $inquiry["respon_status"] = false;
        else
        {
            $jak->addISO(substr($inquiry["respon_http"], 4, strlen($inquiry["respon_http"])));
            $inquiry["data_iso"] = $jak->getData();
            $inquiry["respon_status"] = true;
        }

        return $inquiry;
    }

    /** Function To Process Request Payment PDAM*/
    public function GetPaymentPDAM($no_pelanggan, $pdam_code, $respon_msg)
    {
        $number  = array("2",  "3",  "7",  "11", "12", "15", "22", "33", "35", "37", "39", "42", "43", "59", "60", "61", "63");
        $payment = array();

        require APPPATH. "/libraries/JAK8583/JAK8583.php";

        $jak = new JAK8583();
        $pay = new JAK8583();

        $jak->addISO(substr($respon_msg, 4, strlen($respon_msg)));
        $data = $jak->getData();
        $data = $this->ToUnsetDataISO($data, $number);
        $random = mt_rand(1000, 999999);
        $bit7p = gmdate("mdHis");
        $pay->addMTI("0200");
        $pay->addData(3, $this->PROCESSING_CODE["payment_cash"]);
        $pay->addData(7, $bit7p);
        $pay->addData(11, $random);
        $pay->addData(12, date("His"));
        $pay->addData(15, date("md", strtotime(date("md")) + 86400));
        $pay->addData(37, date("His").$random);
        foreach ($data as $key => $val)
        {
            $pay->addData($key, $val);
        }
        $pay->addData(63, $pdam_code);

        $payment["pesan_iso"]   = $this->ToAddZero(strlen($pay->getISO())).$pay->getISO();
        $payment["respon_http"] = $this->GetHTTPCurl($this->ip_port.urlencode($payment["pesan_iso"])."&stan=".$random."&mType=".'0200');

        if($payment["respon_http"] == "" || $payment["respon_http"] == false)
        {
            $payment["respon_status"] = false;
            $payment["pesan_error"] = "Proses Payment Gagal. No Pelanggan ". $no_pelanggan . " tidak masuk dalam cart";
        }
        else
        {
            $payment["respon_status"] = true;
            $jak->addISO(substr($payment["respon_http"], 4, strlen($payment["respon_http"])));
            $pay->addISO(substr($payment["respon_http"], 4, strlen($payment["respon_http"])));

	        $payment["data_iso_pay"] = $pay->getData();
            $payment["data_iso_jak"] = $jak->getData();
            $payment["kode_fortuna"] = $pdam_code;
            $payment["bit_iso7"]     = $bit7p;
            $payment["random_stan"]  = $random;
            $payment["no_pelanggan"] = $no_pelanggan;
            $payment["respon_http_inquiry"] = $respon_msg;
        }

        return $payment;
    }
}
