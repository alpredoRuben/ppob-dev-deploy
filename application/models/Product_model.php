<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{
    protected $tmp_product = "temp_produk";
    protected $inm_product = "inm_produk";

    public function insertProduct($data)
    {
        $insert = $this->db->insert($this->tmp_product, $data);
        if(!$insert)
            return false;

        return $insert;
    }

    public function getRecordProductAll()
    {
        $query = $this->db->get($this->tmp_product);

        if ($query->num_rows() > 0)
            return $query->result_array();

        return null;
    }

}
