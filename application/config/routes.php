<?php defined('BASEPATH') OR exit('No direct script access allowed');


/** PPOB Rest Server API Serve */
/*$route["rest/get/pdam/inquiry/(:any)/(:any)"] = "ppob_serve/PDAM_Services/Inquiry/$1/$2"; //PDAM Inquiry API Serve
$route["rest/get/pdam/payment/(:any)/(:any)"] = 'ppob_serve/PDAM_Services/Payment/$1/$2'; //PDAM Payment API Serve
$route["rest/get/bpjs/inquiry/(:any)/(:any)/(:any)"] = "ppob_serve/BPJS_Services/inquiry/$1/$2/$3"; //BPJS Inquiry API Serve
$route["rest/get/bpjs/payment/(:any)/(:any)/(:any)"] = "ppob_serve/BPJS_Services/payment/$1/$2/$3"; //BPJS Payment API Serve
*/

/** Record Generate Seeder and Migration **/
$route["seed/add/record/pdam/product"]  = "Tools/setRecordDatabasePDAMPoduct";
$route["seed/add/record/bpjs/product"]  = "Tools/setRecordDatabaseBPJSPoduct";
$route["seed/view/record/product"] = "Tools/getRecordDatabaseProduct";


/** Route Version BPJS 2.0 dan PDAM 2.9 */
$route["rest/get/pdam/inquiry/(:any)/(:any)"] = "rest_server/PDAM/Inquiry/$1/$2"; //PDAM Inquiry API Serve
$route["rest/get/pdam/payment/(:any)/(:any)"] = 'rest_server/PDAM/Payment/$1/$2'; //PDAM Payment API Serve
$route["rest/get/bpjs/inquiry/(:any)/(:any)/(:any)"] = "rest_server/BPJS/inquiry/$1/$2/$3"; //BPJS Inquiry API Serve
$route["rest/get/bpjs/payment/(:any)/(:any)/(:any)"] = "rest_server/BPJS/payment/$1/$2/$3"; //BPJS Payment API Serve


/** PPOB Controller Handle */
$route["pdam/inquiry"] = "PDAM_Controller/pdam_inquiry";
$route["pdam/payment"] = "PDAM_Controller/pdam_payment";
$route["bpjs/inquiry"] = "BPJS_Controller/bpjs_inquiry";
$route["bpjs/payment"] = "BPJS_Controller/bpjs_payment";

$route['bpjs/beranda'] = 'BPJS_Controller/index';
$route['default_controller'] = 'PDAM_Controller/index' ;
$route['404_override'] 	= '';
$route['translate_uri_dashes'] 	= TRUE;
