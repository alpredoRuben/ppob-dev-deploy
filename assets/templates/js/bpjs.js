'use strict'

var inputNoPelanggan = $("input[name=no_pelanggan]");
var inputNamaProduk  = $("select[name=nama_produk]");
var selJumlahBulan   = $("select[name=jumlah_bulan]");
var canvasViewers    = $('#canvas');
var tombolCheck      = $("#btn_check");
var tombolBayar      = $('#btn_bayar');

$(document).ready(function() {

    /** To Handle Button Check Pelanggan */
    tombolCheck.on('click', (e) => {
        e.preventDefault();

        var urls = window.base_url + "bpjs/inquiry";

        window.cart = {
            no_pelanggan : inputNoPelanggan.val(),
            nama_produk  : inputNamaProduk.val(),
            periode_bayar: selJumlahBulan.val()
        };

        axios.post(urls, window.cart).then(function (response) {
            console.log("RESPONSE INQUIRY");
            console.log(response);
            var status      = response.data.STATUS;
            var data_json   = response.data.DATA_JSON;
            var ekstraksi     = data_json.DATA_EKSTRAKSI;

            if(status == false)
            {
                if(data_json.RESPONS_HTTP == false)
                    ToGetDialogBox("Request Time Out", data_json.PESAN_KESALAHAN);
                else
                    ToGetDialogBox("Transaksi PDAM Gagal - ["+ data_json.KODE_KESALAHAN +"]", "Maaf, " + data_json.PESAN_KESALAHAN);
            }
            else
            {
                ToGetDialogBox(data_json.PESAN_KESALAHAN.toUpperCase(), "Perminataan Pencarian No. Pelanggan " + ekstraksi.NO_BILL_PELANGGAN + " Berhasil");
                SetBPJSKesehatanDataToCanvas(canvasViewers, data_json, ekstraksi, ekstraksi.TOTAL_PESERTA_BPJS);
            }
        }).catch(function (error) {
            console.log(error);
        });

    })

    /** To Handle Button Check No Pelanggan */
    tombolBayar.on('click', function(events){
        event.preventDefault();
        var urls = window.base_url + 'bpjs/payment';

        axios.post(urls, window.cart).then(function (response) {
            console.log("RESPONSE PAYMENT");
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        });

    });

});
