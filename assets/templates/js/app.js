'use strict'

function ToGetConfirmBox(title, message) {
    $.confirm({
        title: (title != '') ? title:' Successfully',
        content: "<p>" + message + "</p>",
        typeAnimated: true,
        theme:'light',
        animation: 'zoom',
        animationSpeed: '500',
        buttons:{ close: function() {} }
    });
}

function ToGetDialogBox(title, message) {
    $.alert({
        title: (title != '') ? title:'Successfully',
        content: "<p>" + message + "</p>",
        animationSpeed: '500',
        theme:'light',
        smoothContent:true,
        animation: 'RotateY',
        animationSpeed: '500',
        animationBounce: 1.5,
        buttons:{ close: function() {} }
    });
}

function ConvertCurrency(currencies) {
    currencies = parseInt(currencies);

    var	number_string = currencies.toString(),
	    sisa 	= number_string.length % 3,
	    rupiah 	= number_string.substr(0, sisa),
	    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);

    if (ribuan) {
    	var separator = sisa ? '.' : '';
    	rupiah += separator + ribuan.join('.');
    }

    return rupiah;
}

function SetBPJSKesehatanDataToCanvas(canvas, data_json, ekstraksi, lembar)
{
    canvas.empty();
    var contents = '';

    contents += '<div class="clearfix"></div> &nbsp;';
    contents += '<p><span class="text-info">Total Biaya Seluruh Tagihan  : Rp. ';
    contents += ConvertCurrency(ekstraksi.TOTAL_SELURUH_TAGIHAN) + '</span></p>';

    contents += '<div class="row viewsBord">';
        contents += '<div class="col-md-12">';
            contents += '<h5 class="text-center text-info">PEMBAYARAN IURAN '+  data_json.KATEGORI_BPJS.toUpperCase() + '</h5>';
            contents += '<br>';
        contents += '</div>';

        contents += '<div class="col-md-12">';
            contents += '<div class="row">';
                contents += '<div class="col-md-2">MKM Ref</div>';
                contents += '<div class="col-md-8">' + ekstraksi.NO_REFERENSI + '</div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Waktu Transaksi</div>';
                contents += '<div class="col-md-8">' + ekstraksi.WAKTU_REQUEST_INQUIRY + '</div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Nomor VA</div>';
                contents += '<div class="col-md-8">' + ekstraksi.NO_AKUN_VIRTUAL + '</div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Nama Pendaftar</div>';
                contents += '<div class="col-md-8">' + ekstraksi.NAMA_PENDAFTAR_BPJS + '</div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Jumlah Bulan</div>';
                contents += '<div class="col-md-8">' + ekstraksi.BULAN_TAGIHAN + '</div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Jumlah Peserta</div>';
                contents += '<div class="col-md-8">' + lembar + '</div>';
            contents += '</div>';
        contents += '</div>';

        contents += '<div class="clearfix"></div> &nbsp;';

        contents += '<div class="col-md-12">';
            contents += '<div class="row">';
                contents += '<table class="table table-responsive">';
                    contents += '<thead>';
                        contents += '<tr>';
                            contents += '<th>No. Virtual Acount</th>';
                            contents += '<th>Nama Peserta</th>';
                            contents += '<th>Lokasi Registrasi</th>';
                            contents += '<th>Premi Bulan Ini</th>';
                            contents += '<th>Nominal Tagihan</th>';
                        contents += '</tr>';
                    contents += '</thead>';
                    contents += '<tbody>';
                        for (var i = 0; i < lembar; i++)
                        {
                            contents += '<tr>';
                                contents += '<td>'+ ekstraksi.STRUK_INFORMASI[i].ID_PELANGGAN +'</td>';
                                contents += '<td>'+ ekstraksi.STRUK_INFORMASI[i].NAMA_PELANGGAN.toUpperCase() +'</td>';
                                contents += '<td>'+ ekstraksi.STRUK_INFORMASI[i].NAMA_BRANCH.toUpperCase() + '</td>';
                                contents += '<td>'+ ekstraksi.STRUK_INFORMASI[i].BIAYA_PREMI_BULAN_INI +'</td>';
                                contents += '<td>'+ ekstraksi.STRUK_INFORMASI[i].BIAYA_TAGIHAN +'</td>';
                            contents += '</tr>';
                        }
                    contents += '</tbody>';
                contents += '</table>';
            contents += '</div>';
        contents += '</div>';

        contents += '<div class="clearfix"></div> &nbsp;';

        contents += '<div class="col-md-12">';
            contents += '<div class="row">';
                contents += '<div class="col-md-2">Total Biaya Tagihan</div>';
                contents += '<div class="col-md-8"><span class="text-info"> Rp. ' + ConvertCurrency(ekstraksi.JUMLAH_BIAYA_TAGIHAN) + '</span></div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Biaya Administrasi</div>';
                contents += '<div class="col-md-8"><span class="text-info"> Rp. ' + ConvertCurrency(ekstraksi.BIAYA_ADMIN) + '</span></div>';
            contents += '</div>';

            contents += '<div class="row">';
                contents += '<div class="col-md-2">Total Pembayaran</div>';
                contents += '<div class="col-md-8"><span class="text-info"> Rp. ' + ConvertCurrency(ekstraksi.TOTAL_SELURUH_TAGIHAN) + '</span></div>';
            contents += '</div>';
        contents += '</div>';

    contents += '</div>';

    canvas.append(contents);

}

function SetPDAMDataJSONToCanvas(canvas, data_json, ekstraksi, lembar, struk_info)
{
    canvas.empty();
    var contents = '';

    contents += '<p><span class="text-info">Total Biaya Seluruh Admin  : ';
    contents += ConvertCurrency(ekstraksi.total_biaya_admin) + '</span></p>';


    for (var i = 0; i < lembar; i++)
    {
        contents += '<div class="row viewsBord">';
        	contents += '<div class="col-md-12">';
        		contents += '<h5 class="text-center text-info">' + data_json.nama_pdam.toUpperCase();
        		contents += '<br/><small>' + data_json.nama_propinsi + '</small></h5>';
        		contents += '<br>';
        	contents += '</div>';

        	contents += '<div class="col-md-12">';
        		contents += '<div class="row">';
        				contents += '<div class="col-6">';
        					contents += '<div class="row">';
        						contents += '<div class="col-md-4">Info Tagihan</div>';
                                contents += '<div class="col-md-8">' + struk_info[i].info_tagihan + '</div>';
        					contents += '</div>';

        					contents += '<div class="row">';
                                contents += '<div class="col-md-4">No Pelanggan</div>';
                                contents += '<div class="col-md-8">' + ekstraksi.no_pelanggan + '</div>';
        					contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Nama</div>';
                                contents += '<div class="col-md-8">' + ekstraksi.nama_pelanggan + '</div>';
        					contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Alamat</div>';
                                contents += '<div class="col-md-8">' + ekstraksi.alamat + '</div>';
        					contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">No Tarif</div>';
                                contents += '<div class="col-md-8">' + ekstraksi.no_tarif + '</div>';
        					contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Stan Meter Awal</div>';
                                contents += '<div class="col-md-8">' + struk_info[i].stan_meter_awal + '</div>';
        					contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Besar Pemakaian</div>';
                                contents += '<div class="col-md-8">' + struk_info[i].besar_pemakaian + '</div>';
        					contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Stan Meter Akhir</div>';
                                contents += '<div class="col-md-8">' + struk_info[i].stan_meter_akhir + '</div>';
                            contents += '</div>';
        				contents += '</div>';

                        contents += '<div class="col-6">';
                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Waktu Transaksi</div>';
                                contents += '<div class="col-md-8">' + ekstraksi.waktu_transaksi + '</div>';
                            contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Priode</div>';
                                contents += '<div class="col-md-8">' + struk_info[i].periode_tagihan + '</div>';
                            contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Biaya Tagihan</div>';
                                contents += '<div class="col-md-8">' + ConvertCurrency(struk_info[i].biaya_tagihan) + '</div>';
                            contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Biaya Denda</div>';
                                contents += '<div class="col-md-8">' + ConvertCurrency(struk_info[i].biaya_denda) + '</div>';
                            contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Biaya Admin</div>';
                                contents += '<div class="col-md-8">' + ConvertCurrency(ekstraksi.biaya_admin) + '</div>';
                            contents += '</div>';

                            contents += '<div class="row">';
                                contents += '<div class="col-md-4">Total Seluruh Tagihan</div>';
                                contents += '<div class="col-md-8">' + ConvertCurrency(struk_info[i].total_tagihan) + '</div>';
                            contents += '</div>';
                        contents += '</div>';
        		contents += '</div>';
        	contents += '</div>';
        contents += '</div>';

        contents += '<div class="clearfix"></div> &nbsp;';
    }

    canvas.append(contents);

}
