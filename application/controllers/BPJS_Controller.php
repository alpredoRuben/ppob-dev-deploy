<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BPJS_Controller extends FortunaController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //$this->session->sess_destroy();

        $data = array(
            'title' => 'INM PPOB',
            'subtitle' => 'PPOB DEVELOP',
            'headings' => 'BPJS CONTROLLER',
            'inisial_produk' => GetBPJSNames()
        );

        $this->load->view('ppob/bpjs_view', $data);
    }

    public function bpjs_inquiry()
    {
        $post = json_decode(file_get_contents('php://input'), true);

        $no_pelanggan  = $post["no_pelanggan"];
        $nama_produk   = $post["nama_produk"];
        $periode_bayar = $post["periode_bayar"];

        $str_url  = $this->getBPJSInquery($nama_produk, $no_pelanggan, $periode_bayar);
        redirect($str_url);
        // $response = $this->getRestHTTPCurl($str_url);
        //
        // $this->setOutpuJson($response);
    }

    public function bpjs_payment()
    {
        $post = json_decode(file_get_contents('php://input'), true);

        $no_pelanggan  = $post["no_pelanggan"];
        $nama_produk   = $post["nama_produk"];
        $periode_bayar = $post["periode_bayar"];

        $str_url = $this->getBPJSPayment($nama_produk, $no_pelanggan, $periode_bayar);
        redirect($str_url);
        // $response = $this->getRestHTTPCurl($str_url);
        // $this->setOutpuJson($response);
    }


    public function getBPJSInquery($product_name, $customer_id, $number)
    {
        return "rest/get/bpjs/inquiry/". $product_name ."/". $customer_id . "/". $number;
    }

    public function getBPJSPayment($product_name, $customer_id, $number)
    {
        //rest/get/bpjs/payment/(:any)/(:any)/(:any)
        return "rest/get/bpjs/payment/". $product_name ."/". $customer_id . "/". $number;
    }

}
